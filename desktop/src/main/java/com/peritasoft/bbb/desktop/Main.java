package com.peritasoft.bbb.desktop;

import com.badlogic.gdx.backends.lwjgl.LwjglApplication;
import com.badlogic.gdx.backends.lwjgl.LwjglApplicationConfiguration;
import com.peritasoft.bbb.BoatBoomBoom;
import com.peritasoft.bbb.LoopbackTestScreen;

public class Main {
    public static void main(String[] args) {
        LwjglApplicationConfiguration config = new LwjglApplicationConfiguration();
        config.title = "Boat Boom Boom";
        config.width = 800;
        config.height = 480;
        final BoatBoomBoom game = args.length > 0 && args[0].equals("-testing")
                ? new BoatBoomBoom(LoopbackTestScreen::new)
                : new BoatBoomBoom();
        new LwjglApplication(new BoatBoomBoom(), config);
    }
}
