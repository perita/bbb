package com.peritasoft.bbb.network;

import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.esotericsoftware.kryo.Kryo;
import com.peritasoft.bbb.messages.*;

import java.util.ArrayList;

final public class Network {
    private Network() {
        // Nothing to do; this is a “static class”
    }

    public static void register(final Kryo kryo) {
        kryo.register(MapDef.class);
        kryo.register(JoinedGame.class);
        kryo.register(PlayState.class);
        kryo.register(PlayerState.class);
        kryo.register(PlayerState[].class);
        kryo.register(Vector2.class);
        kryo.register(Rectangle.class);
        kryo.register(ArrayList.class);
        kryo.register(IslandDef.class);
        kryo.register(CaptainOrders.class);
        kryo.register(TurnSide.class);
        kryo.register(CannonBallState.class);
        kryo.register(CannonBallState[].class);
    }

    public static int port() {
        int c = 0;
        final String str = "PerIta Boat Boom Boom";
        for (int i = 0; i < str.length(); i++) {
            c += str.charAt(i);
        }
        return c;
    }
}
