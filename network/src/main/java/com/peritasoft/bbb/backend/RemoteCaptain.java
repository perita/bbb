package com.peritasoft.bbb.backend;

import com.badlogic.gdx.utils.Queue;
import com.esotericsoftware.kryonet.Connection;
import com.peritasoft.bbb.messages.CaptainOrders;

class RemoteCaptain extends Connection implements Captain {
    private int playerId = -1;
    private final Queue<CaptainOrders> receivedOrders = new Queue<>();
    private CaptainOrders lastOrders = new CaptainOrders();

    int getPlayerId() {
        return playerId;
    }

    @Override
    public synchronized CaptainOrders nextOrders() {
        if (!receivedOrders.isEmpty()) {
            lastOrders = receivedOrders.removeFirst();
        }
        return lastOrders;
    }

    @Override
    public void join(BackendGamePlay game) {
        if (this.playerId > -1) {
            throw new IllegalStateException("This captain already joined a game");
        }
        this.playerId = game.addCaptain(this);
    }

    synchronized void receiveOrders(final CaptainOrders orders) {
        receivedOrders.addLast(orders);
    }
}
