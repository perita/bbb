package com.peritasoft.bbb.backend;

import com.badlogic.gdx.ScreenAdapter;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.esotericsoftware.kryonet.Server;
import com.peritasoft.bbb.PiecemealMapDefGenerator;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.JoinedGame;
import com.peritasoft.bbb.messages.MapDef;
import com.peritasoft.bbb.messages.PlayState;
import com.peritasoft.bbb.network.Network;

import java.io.IOException;

public class BackendScreen extends ScreenAdapter implements BackendGamePlay.Listener {
    private final Server server;
    private final BackendGamePlay backend;
    private boolean gameStarted = false;

    public BackendScreen() {
        server = new BackendServer();
        Network.register(server.getKryo());
        server.addListener(new Listener.LagListener(20, 30, new NetworkListener()));

        final PiecemealMapDefGenerator generator = new PiecemealMapDefGenerator();
        final MapDef mapDef = generator.generate(1024, 1024);
        backend = new BackendGamePlay(2, mapDef, this);
        WandererCaptain captain = new WandererCaptain(backend.getWorld());
        captain.join(backend);
    }

    @Override
    public void stateUpdated(PlayState state) {
        server.sendToAllTCP(state);
    }

    @Override
    public void render(float delta) {
        if (!gameStarted) return;
        backend.update(delta);
    }

    @Override
    public void show() {
        try {
            server.start();
            server.bind(Network.port());
        } catch (IOException e) {
            System.err.println("Server could not start: " + e.getMessage());
            System.err.println("Now what?");
        }
    }

    @Override
    public void hide() {
        server.stop();
    }

    @Override
    public void dispose() {
        backend.dispose();
    }

    private void joinCaptain(final RemoteCaptain captain) {
        captain.join(backend);
        if (backend.areAllPiratesJoined()) {
            startGame();
        }
    }

    private void startGame() {
        JoinedGame joinedGame = new JoinedGame(backend.getMapDef(), backend.snapshotState());
        Connection[] connections = server.getConnections();
        for (Connection connection : connections) {
            RemoteCaptain captain = (RemoteCaptain) connection;
            joinedGame.setPlayerId(captain.getPlayerId());
            captain.sendTCP(joinedGame);
        }
        gameStarted = true;
    }

    private class NetworkListener extends Listener {
        @Override
        public void connected(Connection connection) {
            joinCaptain((RemoteCaptain) connection);
        }

        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof CaptainOrders) {
                RemoteCaptain captain = (RemoteCaptain) connection;
                captain.receiveOrders((CaptainOrders) object);
            }
        }
    }

    private static class BackendServer extends Server {
        @Override
        protected Connection newConnection() {
            return new RemoteCaptain();
        }
    }

}
