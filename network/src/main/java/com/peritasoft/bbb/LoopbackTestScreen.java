package com.peritasoft.bbb;

import com.badlogic.gdx.ScreenAdapter;
import com.peritasoft.bbb.backend.BackendScreen;
import com.peritasoft.bbb.frontend.FrontendScreen;

public class LoopbackTestScreen extends ScreenAdapter {
    private final BackendScreen backendScreen;
    private final FrontendScreen frontendScreen;


    public LoopbackTestScreen(final BoatBoomBoom game) {
        backendScreen = new BackendScreen();
        frontendScreen = new FrontendScreen(game);
    }

    @Override
    public void render(final float delta) {
        backendScreen.render(delta);
        frontendScreen.render(delta);
    }

    @Override
    public void show() {
        backendScreen.show();
        frontendScreen.show();
    }

    @Override
    public void hide() {
        frontendScreen.hide();
        backendScreen.hide();
    }

    @Override
    public void resize(final int width, final int height) {
        frontendScreen.resize(width, height);
    }

    @Override
    public void dispose() {
        backendScreen.dispose();
        frontendScreen.dispose();
    }
}
