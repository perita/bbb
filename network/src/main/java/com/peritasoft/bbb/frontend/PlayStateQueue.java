package com.peritasoft.bbb.frontend;

import com.peritasoft.bbb.messages.PlayState;

import java.util.Iterator;
import java.util.concurrent.ConcurrentLinkedQueue;

class PlayStateQueue extends ConcurrentLinkedQueue<PlayState> {
    private final static long STATE_DELAY = 100;

    private final long firstServerTimestamp;
    private final long firstClientTimestamp = System.currentTimeMillis();

    PlayStateQueue(final PlayState initialState) {
        firstServerTimestamp = initialState.getTimestamp();
        offer(initialState);
    }

    PlayState getCurrentState() {
        final long serverTime = currentServerTime();
        final PlayState base = getBaseState(serverTime);
        final PlayState next = getNextState();
        if (next == null) {
            return base;
        }
        final long delta = next.getTimestamp() - base.getTimestamp();
        if (delta == 0) {
            return base;
        }
        final float t = (float) (serverTime - base.getTimestamp()) / (float) delta;
        return PlayState.interpolateBetween(base, next, t);
    }

    PlayState getBaseState() {
        return getBaseState(currentServerTime());
    }

    private PlayState getBaseState(long serverTime) {
        final Iterator<PlayState> iter = iterator();
        iter.next(); // Skip the first because we need to check if the *next* has a suitable timestamp to remove old entries
        int toPop = 0;
        while (iter.hasNext() && iter.next().getTimestamp() <= serverTime) {
            toPop++;
        }
        while (toPop > 0) {
            poll();
            toPop--;
        }
        return peek();
    }

    // Must be called after getBaseState for it to work
    private PlayState getNextState() {
        final Iterator<PlayState> iter = iterator();
        iter.next();
        if (iter.hasNext()) {
            return iter.next();
        }
        return null;
    }

    private long currentServerTime() {
        return firstServerTimestamp + (System.currentTimeMillis() - firstClientTimestamp) - STATE_DELAY;
    }
}
