package com.peritasoft.bbb.frontend;

import com.badlogic.gdx.utils.Queue;
import com.peritasoft.bbb.AbstractGamePlay;
import com.peritasoft.bbb.Boat;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.MapDef;
import com.peritasoft.bbb.messages.PlayState;

final class FrontendGamePlay extends AbstractGamePlay {
    private final Player player;
    private final PlayStateQueue receivedStates;
    private final Queue<CaptainOrders> givenOrders = new Queue<>();

    FrontendGamePlay(final Player player, final MapDef mapDef, final PlayState initialState) {
        super(initialState.countPlayers(), mapDef);
        this.player = player;
        this.receivedStates = new PlayStateQueue(initialState);
        initialState.restore(boats);
    }

    Boat getPlayerBoat() {
        return getBoat(getPlayerBoatIndex());
    }

    private int getPlayerBoatIndex() {
        return player.getBoatIndex();
    }

    public float getWidth() {
        return map.getWidth();
    }

    public float getHeight() {
        return map.getHeight();
    }

    @Override
    protected void beforeSimulation() {
        final PlayState base = receivedStates.getBaseState();
        base.target(boats);
        base.restoreOnly(boats, getPlayerBoatIndex());
        Iterable<CaptainOrders> ordersToReplay = player.getOrdersToReplayFrom(base.getSequenceNumber());
        for (CaptainOrders orders : ordersToReplay) {
            getPlayerBoat().followOrders(orders);
            simulateWorld();
        }
        final PlayState snapshot = receivedStates.getCurrentState();
        snapshot.restoreAllBut(boats, getPlayerBoatIndex());
        base.restore(cannonBalls, world);
        givenOrders.clear();
    }

    @Override
    protected void simulate(final float dt) {
        for (int i = 0; i < boats.length; i++) {
            final Boat boat = boats[i];
            if (i == getPlayerBoatIndex()) {
                final CaptainOrders orders = player.giveOrders();
                givenOrders.addLast(orders);
                boat.followOrders(orders);
            } else {
                boat.moveTowardsTarget();
            }
        }
    }

    void processState(final PlayState snapshot) {
        receivedStates.offer(snapshot);
    }

    Iterable<CaptainOrders> getGivenOrders() {
        return givenOrders;
    }
}
