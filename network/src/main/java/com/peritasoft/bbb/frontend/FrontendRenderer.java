package com.peritasoft.bbb.frontend;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.bbb.Boat;

class FrontendRenderer implements Disposable {
    private final Box2DDebugRenderer box2DDebugRenderer = new Box2DDebugRenderer();
    private final Batch batcher;
    private final ShapeRenderer renderer;
    private final static Vector2[] vertices = new Vector2[1000];

    FrontendRenderer(final Batch batcher) {
        this.batcher = batcher;
        renderer = new ShapeRenderer();
        for (int i = 0; i < vertices.length; i++) {
            vertices[i] = new Vector2();
        }
    }

    void render(final FrontendGamePlay play, final Boat boat, final Viewport viewport) {
        viewport.apply();
        final Camera cam = viewport.getCamera();
        final Vector2 pos = boat.getPosition();
        final float worldWidth = viewport.getWorldWidth();
        final float worldHeight = viewport.getWorldHeight();
        cam.position.x = MathUtils.clamp(pos.x, worldWidth / 2.0f, play.getWidth() - worldWidth / 2.0f);
        cam.position.y = MathUtils.clamp(pos.y, worldHeight / 2.0f, play.getHeight() - worldHeight / 2.0f);
        cam.update();
        batcher.setProjectionMatrix(cam.combined);

        box2DDebugRenderer.render(play.getWorld(), cam.combined);
        final Boat[] boats = play.getBoats();
        renderer.setProjectionMatrix(cam.combined);
        renderer.begin(ShapeRenderer.ShapeType.Line);
        for (int i = 0; i < boats.length; i++) {
            renderTarget(boats[i]);
        }
        renderer.end();
    }

    private void renderTarget(final Boat boat) {
        final Transform transform = boat.getTargetTransform();
        final Body body = boat.getBody();
        for (final Fixture fixture : body.getFixtureList()) {
            drawShape(fixture, transform);
        }
    }

    private void drawShape(final Fixture fixture, final Transform transform) {
        if (fixture.getType() == Shape.Type.Edge) {
            EdgeShape edge = (EdgeShape) fixture.getShape();
            edge.getVertex1(vertices[0]);
            edge.getVertex2(vertices[1]);
            transform.mul(vertices[0]);
            transform.mul(vertices[1]);
            drawSolidPolygon(2, true);
            return;
        }

        if (fixture.getType() == Shape.Type.Polygon) {
            PolygonShape chain = (PolygonShape) fixture.getShape();
            int vertexCount = chain.getVertexCount();
            for (int i = 0; i < vertexCount; i++) {
                chain.getVertex(i, vertices[i]);
                transform.mul(vertices[i]);
            }
            drawSolidPolygon(vertexCount, true);
            return;
        }

        if (fixture.getType() == Shape.Type.Chain) {
            ChainShape chain = (ChainShape) fixture.getShape();
            int vertexCount = chain.getVertexCount();
            for (int i = 0; i < vertexCount; i++) {
                chain.getVertex(i, vertices[i]);
                transform.mul(vertices[i]);
            }
            drawSolidPolygon(vertexCount, false);
        }
    }

    private final Vector2 lv = new Vector2();
    private final Vector2 f = new Vector2();

    private void drawSolidPolygon(int vertexCount, boolean closed) {
        renderer.setColor(1f, 0f, 1f, 1f);
        lv.set(vertices[0]);
        f.set(vertices[0]);
        for (int i = 1; i < vertexCount; i++) {
            Vector2 v = vertices[i];
            renderer.line(lv.x, lv.y, v.x, v.y);
            lv.set(v);
        }
        if (closed) renderer.line(f.x, f.y, lv.x, lv.y);
    }

    @Override
    public void dispose() {
        renderer.dispose();
        box2DDebugRenderer.dispose();
    }
}
