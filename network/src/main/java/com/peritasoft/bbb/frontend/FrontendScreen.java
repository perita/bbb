package com.peritasoft.bbb.frontend;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.esotericsoftware.kryonet.Client;
import com.esotericsoftware.kryonet.Connection;
import com.esotericsoftware.kryonet.Listener;
import com.peritasoft.bbb.BoatBoomBoom;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.JoinedGame;
import com.peritasoft.bbb.messages.PlayState;
import com.peritasoft.bbb.network.Network;

import java.io.IOException;

public class FrontendScreen extends ScreenAdapter {
    private final Viewport viewport;
    private final Client client;
    private FrontendGamePlay frontend;
    private final FrontendRenderer renderer;

    private static final float WORLD_WIDTH = 1000f;
    private static final float WORLD_HEIGHT = 600f;

    public FrontendScreen(final BoatBoomBoom game) {
        viewport = new FitViewport(WORLD_WIDTH, WORLD_HEIGHT);
        renderer = new FrontendRenderer(game.getBatcher());
        client = new Client();
        Network.register(client.getKryo());
        client.addListener(new Listener.LagListener(20, 30, new NetworkListener()));
    }

    @Override
    public void render(float delta) {
        if (frontend == null) return;
        frontend.update(delta);
        for (CaptainOrders orders : frontend.getGivenOrders()) {
            client.sendTCP(orders);
        }

        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render(frontend, frontend.getPlayerBoat(), viewport);
    }

    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
    }

    @Override
    public void dispose() {
        renderer.dispose();
        if (frontend != null) {
            frontend.dispose();
        }
    }

    private void initializeFrontend(final JoinedGame game) {
        final Player.Keys arrowKeys = new Player.Keys(
                Input.Keys.DPAD_UP,
                Input.Keys.DPAD_RIGHT,
                Input.Keys.DPAD_LEFT,
                Input.Keys.SPACE
        );
        frontend = new FrontendGamePlay(new Player(game.getPlayerId(), arrowKeys), game.getMap(), game.getInitialState());
    }

    private class NetworkListener extends Listener {
        @Override
        public void received(Connection connection, Object object) {
            if (object instanceof JoinedGame) {
                initializeFrontend((JoinedGame) object);
            } else if (object instanceof PlayState) {
                updateState((PlayState) object);
            }
        }
    }

    private void updateState(PlayState state) {
        frontend.processState(state);
    }

    @Override
    public void show() {
        client.start();
        try {
            client.connect(5000, "localhost", Network.port());
        } catch (IOException ex) {
            client.stop();
            System.err.println("Client could not connect:" + ex.getMessage());
            System.err.println("Now what?");
        }
    }

    @Override
    public void hide() {
        client.stop();
    }
}
