package com.peritasoft.bbb.frontend;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;
import com.badlogic.gdx.utils.Queue;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.TurnSide;

class Player {
    private final int boatIndex;
    private final Keys keys;
    private final Pool<CaptainOrders> ordersPool = Pools.get(CaptainOrders.class);
    private final Queue<CaptainOrders> givenOrders = new Queue<>();
    private int orderSequenceNumber = 1;

    Player(final int boatIndex, final Keys keys) {
        this.boatIndex = boatIndex;
        this.keys = keys;
    }

    int getBoatIndex() {
        return boatIndex;
    }

    CaptainOrders giveOrders() {
        CaptainOrders orders = ordersPool.obtain();
        orders.setSequenceNumber(orderSequenceNumber++);
        orders.setAdvance(Gdx.input.isKeyPressed(keys.advance));
        if (Gdx.input.isKeyPressed(keys.turnStarboard)) orders.setTurnSide(TurnSide.STARBOARD);
        else if (Gdx.input.isKeyPressed(keys.turnPort)) orders.setTurnSide(TurnSide.PORT);
        else orders.setTurnSide(TurnSide.NONE);
        orders.setFire(Gdx.input.isKeyJustPressed(keys.shoot));
        givenOrders.addLast(orders);
        return orders;
    }

    Iterable<CaptainOrders> getOrdersToReplayFrom(int lastReceivedSequenceNumber) {
        while (!givenOrders.isEmpty() && givenOrders.first().getSequenceNumber() <= lastReceivedSequenceNumber) {
            ordersPool.free(givenOrders.removeFirst());
        }
        return givenOrders;
    }

    static class Keys {
        final int advance;
        final int turnStarboard;
        final int turnPort;
        final int shoot;

        Keys(final int advance, final int turnStarboard, final int turnPort, final int shoot) {
            this.advance = advance;
            this.turnStarboard = turnStarboard;
            this.turnPort = turnPort;
            this.shoot = shoot;
        }

    }
}
