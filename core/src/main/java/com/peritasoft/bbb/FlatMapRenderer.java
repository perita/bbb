package com.peritasoft.bbb;

public class FlatMapRenderer extends MapRenderer {
    @Override
    public int colorOf(int value) {
        if (value >= 0 && value <= 116) return (0x337979 << 8) | 255;
        else if (value >= 117 && value <= 126) return (0x43a0a0 << 8) | 255;
        else if (value >= 127 && value <= 149) return (0xe0dcad << 8) | 255;
        return (0x668921 << 8) | 255;
    }
}
