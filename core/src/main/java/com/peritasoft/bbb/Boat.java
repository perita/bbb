package com.peritasoft.bbb;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.TurnSide;

public class Boat implements Steerable {
    // BEAM = (LOA + 2.6847) / 3.3255 + 1
    // See “Anteproyecto catamarán de pasajeros menor fabricado en aluminio para sector del Río de la plata”, p. 11
    private static final float LENGTH_OVERALL = 25f;
    private static final float BEAM = 9f;
    private static final float ADVANCE = 500f;
    private static final float TORQUE = 160f;

    private final Body body;
    private final Transform target = new Transform();
    private final CaptainOrders targetOrders = new CaptainOrders();
    private final Vector2 linearVelocityToApply = new Vector2(ADVANCE, 0f);

    Boat(final World world) {
        body = createBody(world);
    }

    public float getAngle() {
        return body.getAngle();
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    public void placeOn(final Vector2 position, final float angle) {
        body.setTransform(position, angle);
    }

    public Vector2 getLinearVelocity() {
        return body.getLinearVelocity();
    }

    public void setLinearVelocity(final Vector2 linearVelocity) {
        body.setLinearVelocity(linearVelocity);
    }

    public float getAngularVelocity() {
        return body.getAngularVelocity();
    }

    public void setAngularVelocity(final float angularVelocity) {
        body.setAngularVelocity(angularVelocity);
    }

    public void targetTo(Vector2 position, float angle) {
        target.setPosition(position);
        target.setRotation(angle);
    }

    public void moveTowardsTarget() {
        targetOrders.setAdvance(!MathUtils.isEqual(getPosition().dst2(target.getPosition()), 0f));
        if (MathUtils.isEqual(getAngle(), target.getRotation())) {
            targetOrders.setTurnSide(TurnSide.NONE);
        } else if (getAngle() < target.getRotation()) {
            targetOrders.setTurnSide(TurnSide.PORT);
        } else {
            targetOrders.setTurnSide(TurnSide.STARBOARD);
        }
        followOrders(targetOrders);
    }

    public void followOrders(final CaptainOrders orders) {
        if (orders.isAdvance()) {
            body.applyLinearImpulse(body.getWorldVector(linearVelocityToApply), getPosition(), true);
        }
        switch (orders.getTurnSide()) {
            case PORT:
                body.setAngularVelocity(1f);
                break;
            case STARBOARD:
                body.setAngularVelocity(-1f);
                break;
            case NONE:
                body.setAngularVelocity(0f);
                break;
        }
    }

    private Body createBody(final World world) {
        final BodyDef def = new BodyDef();
        def.type = BodyDef.BodyType.DynamicBody;

        final Body body = world.createBody(def);
        body.setUserData(this);
        body.setLinearDamping(.5f);
        body.setAngularDamping(1f);

        final PolygonShape box = new PolygonShape();
        box.setAsBox(LENGTH_OVERALL / 2f, BEAM / 2f);

        final FixtureDef fixture = new FixtureDef();
        fixture.shape = box;
        fixture.density = .4f;
        fixture.isSensor = false;
        fixture.restitution = .2f;

        body.createFixture(fixture);
        box.dispose();
        return body;
    }

    public Body getBody() {
        return body;
    }

    public Transform getTargetTransform() {
        return target;
    }
}
