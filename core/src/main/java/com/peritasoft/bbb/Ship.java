package com.peritasoft.bbb;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class Ship implements Steerable {
    public static final float RELOAD_TIME = 2f;

    private static final float THRUST = 500f;
    private static final float TORQUE = 160f;
    private static final float MAX_IMMORTAL_TIME = 3f;
    private static final Vector2 AFT_LOCAL_POS = new Vector2(-20f, 0f);

    private final Sailor sailor;
    private final int faction;

    private final Body body;
    private int ammo;
    private int life;
    private final SpawnTimer ripplesSpawner;
    private boolean isShooting;
    private float reload;
    private float immortalTime;
    private Ship shipFollowed;

    public Ship(final Vector2 initialPosition, final Sailor sailor, int faction, final World world) {
        this.sailor = sailor;
        this.faction = faction;

        body = createBody(this, initialPosition, new Vector2(25f / 2f, 14f / 2f), world);
        ammo = 0;
        life = 2;
        ripplesSpawner = new SpawnTimer(0.015f);
        isShooting = false;
        reload = 0f;
        immortalTime = 0f;
        shipFollowed = null;

        sailor.setShip(this);
    }

    @Override
    public Vector2 getLinearVelocity() {
        return body.getLinearVelocity();
    }

    @Override
    public Vector2 getPosition() {
        return body.getPosition();
    }

    public Vector2 getAftPosition() {
        return body.getWorldPoint(AFT_LOCAL_POS);
    }

    public Body getBody() {
        return body;
    }

    public float getReload() {
        return reload;
    }

    public boolean isShooting() {
        return isShooting;
    }

    public int getFaction() {
        return faction;
    }

    public float getImmortalTime() {
        return immortalTime;
    }

    public Ship getShipFollowed() {
        return shipFollowed;
    }

    public void setShipFollowed(Ship ship) {
        this.shipFollowed = ship;
    }

    public float getAngle() {
        return MathUtils.radiansToDegrees * body.getAngle();
    }

    public boolean isAlive() {
        return life > 0;
    }

    public boolean isImmortal() {
        return immortalTime > 0;
    }

    public boolean isMoving() {
        return body.getLinearVelocity().len() > 10f;
    }

    public boolean update(float delta, GamePlay play) {
        if (!isAlive()) return true;

        sailor.update(delta);

        ripplesSpawner.update(delta, () -> {
            if (isMoving()) {
                play.addRipple(this);
            }
        });

        Vector2 linearVelocity = new Vector2(sailor.thrust() * THRUST, 0f);
        body.applyLinearImpulse(body.getWorldVector(linearVelocity), getPosition(), true);

        body.setTransform(
                body.getWorldCenter(),
                body.getAngle() + sailor.rotation() * MathUtils.degreesToRadians * TORQUE * delta
        );
        body.setAngularDamping(1f);

        if (isImmortal()) {
            immortalTime -= delta * 3f;
        } else {
            immortalTime = 0f;
        }

        if (ammo > 0) {
            if (reload < RELOAD_TIME) {
                reload += delta;
            } else {
                reload = RELOAD_TIME;
            }
        } else {
            reload = 0f;
        }

        if (shoot()) {
            Vector2 perpendicular = new Vector2(1f, 0f).rotate(getAngle() + 90);
            play.addBullet(this, perpendicular);
            isShooting = true;
        } else {
            isShooting = false;
        }

        return false;
    }

    public void die() {
        life = 0;
    }

    public int getAmmo() {
        return ammo;
    }

    public void addAmmo(int i) {
        ammo += i;
    }

    public int getLife() {
        return life;
    }

    public void takeDamage() {
        if (!isImmortal()) {
            life -= 1;
            immortalTime = MAX_IMMORTAL_TIME;
        }
    }

    public void heal() {
        if (life < 3) life += 1;
    }

    public boolean isAmmoLoaded() {
        return reload == RELOAD_TIME;
    }

    private boolean shoot() {
        if (ammo > 0 && isAmmoLoaded()) {
            if (sailor.shootCanon()) {
                ammo -= 1;
                reload = 0f;
                return true;
            }
        }
        return false;
    }

    public void renderDebug(Matrix4 projectionMatrix) {
        sailor.renderDebug(projectionMatrix);
    }

    private static Body createBody(final Ship ship, final Vector2 initialPosition, final Vector2 size, final World world) {
        BodyDef def = new BodyDef();
        def.position.set(initialPosition);
        def.angle = MathUtils.random(0f, 360f) * MathUtils.degreesToRadians;
        def.type = BodyDef.BodyType.DynamicBody;

        Body body = world.createBody(def);
        body.setUserData(ship);
        body.setLinearDamping(0.5f);

        PolygonShape box = new PolygonShape();
        box.setAsBox(size.x, size.y);

        FixtureDef fixture = new FixtureDef();
        fixture.shape = box;
        fixture.density = 0.4f;
        fixture.isSensor = false;
        fixture.restitution = 0.2f;

        body.createFixture(fixture);
        box.dispose();
        return body;
    }
}
