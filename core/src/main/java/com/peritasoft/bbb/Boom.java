package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;

public class Boom {
    private final Vector2 position;
    private float rotation;
    private final SpawnTimer rotationUpdater;
    private float life;

    public Boom(Vector2 position) {
        this.position = position;
        rotation = 0f;
        rotationUpdater = new SpawnTimer(0.15f);
        life = 1f;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getRotation() {
        return rotation;
    }

    public boolean update(float delta) {
        rotationUpdater.update(delta, () -> rotation = (45f - rotation));
        life -= delta;
        return life <= 0f;
    }
}
