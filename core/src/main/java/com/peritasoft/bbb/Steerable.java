package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;

public interface Steerable {
    Vector2 getLinearVelocity();

    Vector2 getPosition();
}
