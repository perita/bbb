package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class CannonBall {
    private boolean active = false;
    private Body body = null;
    private static final float SPEED = 200f;

    public boolean isActive() {
        return active;
    }

    public void reset(final Vector2 position, final Vector2 direction, final World world) {
        this.body = createBody(position, direction, world);
        active = true;
    }

    public void setPosition(final Vector2 position) {
        assert body != null;
        body.setTransform(position, 0f);
    }

    public void setLinearVelocity(final Vector2 linearVelocity) {
        assert body != null;
        body.setLinearVelocity(linearVelocity);
    }

    private Body createBody(final Vector2 initialPosition, final Vector2 direction, final World world) {
        BodyDef def = new BodyDef();
        def.position.set(initialPosition);
        def.type = BodyDef.BodyType.DynamicBody;
        def.linearVelocity.set(direction.x * SPEED, direction.y * SPEED);
        Body body = world.createBody(def);
        body.setUserData(this);

        CircleShape circle = new CircleShape();
        circle.setRadius(3f);

        FixtureDef fixture = new FixtureDef();
        fixture.shape = circle;
        fixture.density = 0.4f;
        fixture.isSensor = false;
        fixture.restitution = 0.2f;

        body.createFixture(fixture);
        circle.dispose();
        return body;
    }

    public Vector2 getPosition() {
        assert body != null;
        return body.getPosition();
    }

    public Vector2 getLinearVelocity() {
        assert body != null;
        return body.getLinearVelocity();
    }
}
