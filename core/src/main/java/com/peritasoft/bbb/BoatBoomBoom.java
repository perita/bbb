package com.peritasoft.bbb;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

public class BoatBoomBoom extends Game {
    private SpriteBatch batcher;
    private final FirstScreenBuilder firstScreen;

    public BoatBoomBoom() {
        this(PeritaSoftScreen::new);
    }

    public BoatBoomBoom(final FirstScreenBuilder firstScreen) {
        this.firstScreen = firstScreen;
    }

    @Override
    public void create() {
        batcher = new SpriteBatch();
        setScreen(firstScreen.build(this));
    }

    public Batch getBatcher() {
        return batcher;
    }

    @Override
    public void dispose() {
        super.dispose();
        if (screen != null) screen.dispose();
        batcher.dispose();
    }

    public interface FirstScreenBuilder {
        Screen build(BoatBoomBoom game);
    }
}