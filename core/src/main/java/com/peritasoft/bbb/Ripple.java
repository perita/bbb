package com.peritasoft.bbb;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Pool;

public class Ripple implements Pool.Poolable {

    private float life;
    private final Vector2 position;
    private final Vector2 facing;
    private float scale;

    public Ripple() {
        life = 0.0f;
        position = new Vector2(0.0f, 0.0f);
        facing = new Vector2(0.0f, 0.0f);
        scale = 1.0f;

    }

    public void update(float delta) {
        life -= delta * 16.0f;
        scale += delta / 2.0f;
        position.add(facing.x * delta * 10.0f, facing.y * delta * 10.0f);
    }

    public void init(Vector2 position, float angle) {
        this.life = 15.0f;
        final float shake_limits = 10.0f;
        final float shake = MathUtils.random(-shake_limits, shake_limits);
        this.facing.set(-1f, 0f).rotate(angle + shake / shake_limits * 90f);
        this.position.set(position).add(
                -facing.y * shake,
                facing.x * shake
        );
        this.scale = 0.50f;
    }

    @Override
    public void reset() {
        life = 0.0f;
        position.set(0.0f, 0.0f);
        scale = 1.0f;
    }

    public float getLife() {
        return life;
    }

    public Vector2 getPosition() {
        return position;
    }

    public float getScale() {
        return scale;
    }

    public boolean isAlive() {
        return life > 0;
    }
}
