package com.peritasoft.bbb;

public class SpawnTimer {
    private final float period;
    private float time;

    public SpawnTimer(float period) {
        this.period = period;
        this.time = period;
    }

    public void update(float delta, Runnable op) {
        time -= delta;
        if (time <= 0.0f) {
            time += period;
            op.run();
        }
    }
}
