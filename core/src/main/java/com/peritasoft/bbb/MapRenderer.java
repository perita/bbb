package com.peritasoft.bbb;

import com.badlogic.gdx.graphics.Pixmap;

public abstract class MapRenderer {
    public Pixmap renderToPixmap(final Map map) {
        final Pixmap pixmap = new Pixmap(map.getWidth(), map.getHeight(), Pixmap.Format.RGBA8888);
        for (int y = 0; y < map.getHeight(); y++) {
            int pixmapY = map.getHeight() - y;
            for (int x = 0; x < map.getWidth(); x++) {
                pixmap.drawPixel(x, pixmapY, colorOf(map.get(x, y)));
            }
        }
        return pixmap;
    }

    protected abstract int colorOf(int value);
}
