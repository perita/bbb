package com.peritasoft.bbb;

import com.badlogic.gdx.graphics.Color;

import java.util.HashMap;
import java.util.Map;

public class SmoothMapRenderer extends MapRenderer {

    private final Map<Integer, Color> colors;

    public SmoothMapRenderer() {
        colors = new HashMap<>();
        colors.put(255, Color.valueOf("#3d4629"));
        colors.put(204, Color.valueOf("#76854e"));
        colors.put(175, Color.valueOf("#99924e"));
        colors.put(150, Color.valueOf("#a1a44d"));
        colors.put(138, Color.valueOf("#c5c06f"));
        colors.put(135, Color.valueOf("#e1d184"));
        colors.put(130, Color.valueOf("#f8eeca"));
        colors.put(127, Color.valueOf("#dbbb82"));
        colors.put(124, Color.valueOf("#0ac2b8"));
        colors.put(122, Color.valueOf("#088297"));
        colors.put(117, Color.valueOf("#005b82"));
        colors.put(89, Color.valueOf("#002764"));
        colors.put(63, Color.valueOf("#07103b"));
        colors.put(38, Color.valueOf("#07103b"));
        colors.put(0, Color.valueOf("#0b0a2a"));
    }

    @Override
    public int colorOf(int value) {
        int min = -9999;
        int max = 9999;
        for (int key : colors.keySet()) {
            if (key >= (value + 1) && key < max) {
                max = key;
            }
            if (key >= (min + 1) && key < value) {
                min = key;
            }
        }
        float portion = ((float) (value - min)) / ((float) (max - min));
        Color color = colors.get(min).cpy();
        return Color.rgba8888(color.lerp(colors.get(max), portion));
    }
}
