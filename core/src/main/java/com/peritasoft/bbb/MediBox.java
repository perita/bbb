package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class MediBox extends Box {
    public MediBox(final Vector2 position, final World world, float lifeTime) {
        super(position, world, lifeTime);
        getBody().setUserData(this);
    }
}
