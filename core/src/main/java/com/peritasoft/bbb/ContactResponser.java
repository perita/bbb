package com.peritasoft.bbb;

import com.badlogic.gdx.physics.box2d.*;

public class ContactResponser implements ContactListener {
    private final GamePlay play;

    public ContactResponser(final GamePlay play) {
        this.play = play;
    }

    @Override
    public void beginContact(final Contact contact) {
        Fixture fa = contact == null ? null : contact.getFixtureA();
        Fixture fb = contact == null ? null : contact.getFixtureB();
        Object obj = fa != null && fa.getBody() != null ? fa.getBody().getUserData() : null;
        Object other = fb != null && fb.getBody() != null ? fb.getBody().getUserData() : null;
        if (obj instanceof Ship) collisionWith((Ship) obj, other);
        else if (obj instanceof MediBox) collisionWith((MediBox) obj, other);
        else if (obj instanceof BulletBox) collisionWith((BulletBox) obj, other);
        else if (obj instanceof Bullet) collisionWith((Bullet) obj, other);
        else if (obj == null) collisionWithLand(other);
    }

    private void collisionWith(final Ship ship, final Object other) {
        if (other instanceof MediBox) play.collided(ship, (MediBox) other);
        else if (other instanceof BulletBox) play.collided(ship, (BulletBox) other);
        else if (other instanceof Bullet) play.collided(ship, (Bullet) other);
        else if (other == null) play.collidedWithLand(ship);
    }

    private void collisionWith(final MediBox box, final Object other) {
        if (other instanceof Ship) play.collided((Ship) other, box);
    }

    private void collisionWith(final BulletBox box, final Object other) {
        if (other instanceof Ship) play.collided((Ship) other, box);
    }

    private void collisionWith(final Bullet bullet, final Object other) {
        if (other instanceof Ship) play.collided((Ship) other, bullet);
        else if (other == null) play.collidedWithLand(bullet);
    }

    private void collisionWithLand(final Object other) {
        if (other instanceof Ship) play.collidedWithLand((Ship) other);
        else if (other instanceof Bullet) play.collidedWithLand((Bullet) other);
    }

    @Override
    public void endContact(Contact contact) {
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {
    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {
    }
}
