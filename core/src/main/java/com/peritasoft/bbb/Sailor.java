package com.peritasoft.bbb;

import com.badlogic.gdx.math.Matrix4;

public interface Sailor {
    Ship getShip();

    void setShip(Ship ship);

    float thrust();

    float rotation();

    boolean shootCanon();

    void update(float delta);

    void renderDebug(Matrix4 projectionMatrix);
}
