package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;

public abstract class AbstractGamePlay implements Disposable {
    public static final float UPDATE_STEP = 1f / 60f;
    private static final int VELOCITY_ITERATIONS = 6;
    private static final int POSITION_ITERATIONS = 2;

    private float accumulatedTime = 0f;
    protected final World world = new World(Vector2.Zero, true);
    protected final Map map;
    protected final Boat[] boats;
    protected final Array<CannonBall> cannonBalls = new Array<>(false, 48);

    protected AbstractGamePlay(final int numPlayers, final com.peritasoft.bbb.messages.MapDef mapDef) {
        this.map = mapDef.buildMap(this.world);
        boats = new Boat[numPlayers];
        for (int s = 0; s < boats.length; s++) {
            boats[s] = new Boat(this.world);
        }
    }

    public World getWorld() {
        return world;
    }

    public void update(final float dt) {
        accumulatedTime += dt;
        beforeSimulation();
        while (accumulatedTime > UPDATE_STEP) {
            accumulatedTime -= UPDATE_STEP;
            simulateWorld();
            simulate(UPDATE_STEP);
        }
    }

    protected void simulateWorld() {
        world.step(UPDATE_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);
    }

    @Override
    public void dispose() {
        world.dispose();
    }

    public Boat[] getBoats() {
        return boats;
    }

    public Boat getBoat(int id) {
        return boats[id];
    }

    protected void beforeSimulation() {
        // Nothing to do.
    }

    @SuppressWarnings("SameParameterValue")
    abstract protected void simulate(final float dt);
}
