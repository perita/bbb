package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.peritasoft.bbb.ai.WandererSailor;

import java.util.ArrayList;

public class GamePlayScreen extends ScreenAdapter {

    private final BoatBoomBoom game;
    private final int localPlayers;

    private final GamePlay play;
    private final World world;
    private final GamePlayRenderer renderer;
    private final Ship player1;
    private Ship player2;


    public GamePlayScreen(final BoatBoomBoom game, int localPlayers) {
        this.game = game;
        this.localPlayers = localPlayers;

        final KeyboardSailor.Keys arrowKeys = new KeyboardSailor.Keys(
                Input.Keys.DPAD_UP,
                Input.Keys.DPAD_RIGHT,
                Input.Keys.DPAD_LEFT,
                Input.Keys.SPACE
        );
        final KeyboardSailor.Keys WASDKeys = new KeyboardSailor.Keys(
                Input.Keys.W,
                Input.Keys.D,
                Input.Keys.A,
                Input.Keys.CONTROL_LEFT
        );
        world = new World(new Vector2(0f, 0f), true);
        final PiecemealMapDefGenerator generator = new PiecemealMapDefGenerator();
        final Map map = generator.generate(1024, 1024).buildMap(world);
        final ArrayList<Ship> ships = new ArrayList<>();
        ships.add(new Ship(map.findStartingPosition(), new KeyboardSailor(arrowKeys), 0, world));
        if (localPlayers == 1) {
            ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 1, world));
        } else {
            ships.add(new Ship(map.findStartingPosition(), new KeyboardSailor(WASDKeys), 1, world));
        }
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 2, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 2, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 2, world));
        player1 = ships.get(0);
        if (localPlayers == 2) player2 = ships.get(1);
        play = new GamePlay(world, map, ships);
        world.setContactListener(new ContactResponser(play));
        renderer = new GamePlayRenderer(game.getBatcher(), play, localPlayers, ships.get(0));
    }

    @Override
    public void render(float delta) {
        draw();
        update(delta);
        Ship lastShip = play.endGame();
        if (lastShip != null) {
            game.setScreen(new EndGameScreen(game, renderer, localPlayers));
        }
    }

    private void update(float delta) {
        play.update(Math.min(0.25f, delta));
        if (Gdx.input.isKeyJustPressed(Input.Keys.TAB)) {
            renderer.toggleDebug();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_RIGHT)) {
            renderer.followNextBoatP1();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_LEFT)) {
            renderer.followPrevBoatP1();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.D)) {
            renderer.followNextBoatP2();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.A)) {
            renderer.followPrevBoatP2();
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            if (!player1.isAlive() && !(player2 != null && player2.isAlive())) {
                game.setScreen(new StartMenuScreen(game));
            }
        }
    }

    private void draw() {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        renderer.resize(width, height);
    }

    @Override
    public void dispose() {
        super.dispose();
        renderer.dispose();
        world.dispose();
    }

}
