package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.Pool;

public class WaterSplash implements Pool.Poolable {
    private static final float MAX_LIFE = 25.0f;

    float life;
    private final Vector2 position;
    private FloatArray scales;

    public WaterSplash() {
        life = 0f;
        position = new Vector2(0.0f, 0.0f);
        scales = new FloatArray();
        scales.add(1.0f);
    }

    public boolean isAlive() {
        return life > 0;
    }

    public Vector2 getPosition() {
        return position;
    }

    public FloatArray getScales() {
        return scales;
    }

    public void update(float delta) {
        life -= delta * 16.0f;
        for (int i = 0; i < scales.size; i++) {
            scales.set(i, scales.get(i) + delta / 2.0f);
        }
        if (life < MAX_LIFE * 0.75 && scales.size == 1) {
            scales.add(0.20f);
        }
        if (life < MAX_LIFE / 2 && scales.size == 2) {
            scales.add(0.15f);
        }
    }

    public void init(Vector2 position) {
        this.life = MAX_LIFE;
        this.position.set(position);
        this.scales.clear();
        this.scales.add(.50f);
    }

    @Override
    public void reset() {
        life = 0.0f;
        position.set(0f, 0f);
        this.scales.clear();
    }
}
