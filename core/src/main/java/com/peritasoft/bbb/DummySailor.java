package com.peritasoft.bbb;

import com.badlogic.gdx.math.Matrix4;

public class DummySailor implements Sailor {

    private Ship ship;

    @Override
    public Ship getShip() {
        return ship;
    }

    @Override
    public void setShip(Ship ship) {
        this.ship = ship;
    }

    @Override
    public void renderDebug(Matrix4 projectionMatrix) {
        // Nothing to do
    }

    @Override
    public void update(float delta) {
        // Nothing to do
    }

    @Override
    public float thrust() {
        return 0f;
    }

    @Override
    public float rotation() {
        return 0f;
    }

    @Override
    public boolean shootCanon() {
        return false;
    }
}
