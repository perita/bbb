package com.peritasoft.bbb;

import com.badlogic.gdx.ai.GdxAI;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Pool;
import com.badlogic.gdx.utils.Pools;

import java.util.ArrayList;
import java.util.Iterator;

public class GamePlay {
    private final static float BULLETBOX_SPAWN_FREQ = 10f;
    private final static float MEDIBOX_SPAWN_FREQ = 25f;
    private final static float BULLETBOX_LIFETIME = 35f;
    private final static float MEDIBOX_LIFETIME = 40f;
    private final static float PHYSICS_STEP = 1f / 60f;
    private final static int VELOCITY_ITERATIONS = 6;
    private final static int POSITION_ITERATIONS = 2;

    private final World world;
    private final Map map;
    private final ArrayList<Ship> ships;

    private float accumulator;

    private final ArrayList<Box> boxes;
    private final ArrayList<Bullet> bullets;
    private final ArrayList<Ripple> ripples;
    private final ArrayList<WaterSplash> waterSplashes;
    private final ArrayList<Boom> booms;
    private final ArrayList<Dust> dusts;
    private final ArrayList<Explosion> explosions;

    private final Pool<Ripple> ripplePool;
    private final Pool<WaterSplash> waterSplashPool;
    private final SpawnTimer boxSpawner;
    private final SpawnTimer mediboxSpawner;


    public GamePlay(final World world, final Map map, final ArrayList<Ship> ships) {
        this.world = world;
        this.map = map;
        this.ships = ships;

        accumulator = 0f;

        boxes = new ArrayList<>(20);
        bullets = new ArrayList<>(100);
        ripples = new ArrayList<>(500);
        waterSplashes = new ArrayList<>(200);
        booms = new ArrayList<>(1);
        dusts = new ArrayList<>(1);
        explosions = new ArrayList<>(1);

        ripplePool = Pools.get(Ripple.class);
        waterSplashPool = Pools.get(WaterSplash.class);
        boxSpawner = new SpawnTimer(BULLETBOX_SPAWN_FREQ);
        mediboxSpawner = new SpawnTimer(MEDIBOX_SPAWN_FREQ);
    }

    public Map getMap() {
        return map;
    }

    public ArrayList<Ship> getShips() {
        return ships;
    }

    public World getWorld() {
        return world;
    }

    public ArrayList<Ripple> getRipples() {
        return ripples;
    }

    public ArrayList<WaterSplash> getWaterSplashes() {
        return waterSplashes;
    }

    public ArrayList<Boom> getBooms() {
        return booms;
    }

    public ArrayList<Dust> getDusts() {
        return dusts;
    }

    public ArrayList<Explosion> getExplosions() {
        return explosions;
    }

    public ArrayList<Bullet> getBullets() {
        return bullets;
    }

    public int getWidth() {
        return map.getWidth();
    }

    public int getHeight() {
        return map.getHeight();
    }

    public void update(float delta) {
        accumulator += Math.min(0.25f, delta);
        while (accumulator >= PHYSICS_STEP) {
            accumulator -= PHYSICS_STEP;

            GdxAI.getTimepiece().update(delta);

            world.step(PHYSICS_STEP, VELOCITY_ITERATIONS, POSITION_ITERATIONS);

            final Iterator<Ripple> ripplesIter = ripples.iterator();
            while (ripplesIter.hasNext()) {
                final Ripple ripple = ripplesIter.next();
                ripple.update(delta);
                if (!ripple.isAlive()) {
                    ripplePool.free(ripple);
                    ripplesIter.remove();
                }
            }

            final Iterator<WaterSplash> splashesIter = waterSplashes.iterator();
            while (splashesIter.hasNext()) {
                final WaterSplash splash = splashesIter.next();
                splash.update(delta);
                if (!splash.isAlive()) {
                    waterSplashPool.free(splash);
                    splashesIter.remove();
                }
            }


            final Iterator<Boom> boomsIter = booms.iterator();
            while (boomsIter.hasNext()) {
                final Boom boom = boomsIter.next();
                if (boom.update(delta)) {
                    boomsIter.remove();
                }
            }

            final Iterator<Dust> dustsIter = dusts.iterator();
            while (dustsIter.hasNext()) {
                final Dust dust = dustsIter.next();
                if (dust.update(delta)) {
                    dustsIter.remove();
                }
            }

            final Iterator<Explosion> explosionsIter = explosions.iterator();
            while (explosionsIter.hasNext()) {
                final Explosion explosion = explosionsIter.next();
                if (explosion.update(delta)) {
                    explosionsIter.remove();
                }
            }

            final Iterator<Ship> shipsIter = ships.iterator();
            while (shipsIter.hasNext()) {
                final Ship ship = shipsIter.next();
                if (ship.update(delta, this)) {
                    world.destroyBody(ship.getBody());
                    shipsIter.remove();
                }
            }

            final Iterator<Box> boxesIter = boxes.iterator();
            while (boxesIter.hasNext()) {
                final Box box = boxesIter.next();
                if (box.update(delta)) {
                    world.destroyBody(box.getBody());
                    boxesIter.remove();
                }
            }

            boxSpawner.update(delta, () -> boxes.add(
                    new BulletBox(
                            findGoodPositionForBox(),
                            world, MathUtils.random(BULLETBOX_LIFETIME, BULLETBOX_LIFETIME + 13f)
                    )
            ));

            mediboxSpawner.update(delta, () -> boxes.add(
                    new MediBox(
                            findGoodPositionForBox(),
                            world, MathUtils.random(MEDIBOX_LIFETIME, MEDIBOX_LIFETIME + 13f)
                    )
            ));

            final Iterator<Bullet> bulletsIter = bullets.iterator();
            while (bulletsIter.hasNext()) {
                final Bullet bullet = bulletsIter.next();
                if (bullet.update(delta)) {
                    world.destroyBody(bullet.getBody());
                    if (!bullet.getKilled()) {
                        addWaterSplash(bullet);
                    }
                    bulletsIter.remove();
                }
            }
        }
    }

    public void collided(final Ship ship, final MediBox box) {
        if (ship.isAlive() && box.isAlive()) {
            kill(box);
            ship.heal();
        }
    }

    public void collided(final Ship ship, final BulletBox box) {
        if (ship.isAlive() && box.isAlive()) {
            kill(box);
            ship.addAmmo(2);
        }
    }

    public void collided(final Ship ship, final Bullet bullet) {
        if (ship.isAlive()) {
            ship.takeDamage();
            kill(bullet);
            explosions.add(new Explosion(bullet.getPosition().cpy()));
            if (!ship.isAlive()) {
                kill(ship);
            }
        }
    }

    public void collidedWithLand(final Ship ship) {
        kill(ship);
    }

    public void collidedWithLand(final Bullet bullet) {
        kill(bullet);
        final Vector2 pos = bullet.getPosition();
        if (pos.x >= 0f && pos.x <= (float) map.getWidth() &&
                pos.y >= 0f && pos.y <= (float) map.getHeight()
        ) {
            dusts.add(new Dust(pos.cpy()));
        }
    }

    public void addRipple(final Ship ship) {
        final Ripple ripple = ripplePool.obtain();
        ripple.init(ship.getAftPosition(), ship.getAngle());
        ripples.add(ripple);
    }

    public void addWaterSplash(final Bullet bullet) {
        final WaterSplash splash = waterSplashPool.obtain();
        splash.init(bullet.getPosition());
        waterSplashes.add(splash);
    }

    public void addBullet(final Ship ship, final Vector2 direction) {
        final Vector2 opposite = new Vector2(-direction.x, -direction.y);
        float margin = 15f;
        bullets.add(new Bullet(ship.getPosition().cpy().add(direction.x * margin, direction.y * margin), direction, world));
        bullets.add(new Bullet(ship.getPosition().cpy().add(opposite.x * margin, opposite.y * margin), opposite, world));
    }

    private void kill(final Ship ship) {
        ship.die();
        if (ships.get(0) != ship) {
            ship.setShipFollowed(ships.get(0));
        } else {
            ship.setShipFollowed(ships.get(1));
        }
        booms.add(new Boom(ship.getPosition()));
    }

    private void kill(final Box box) {
        box.die();
    }

    private void kill(final Bullet bullet) {
        if (bullet.isAlive()) {
            bullet.die();
        }
    }

    private Vector2 findGoodPositionForBox() {
        Vector2 position;
        do {
            position = map.findStartingPosition();
        } while (!isGoodPositionForBox(position));
        return position;
    }

    private boolean isGoodPositionForBox(final Vector2 position) {
        Array<Body> bodies = new Array<>();
        world.getBodies(bodies);
        Body otherBody = null;
        for (Body body : bodies) {
            if (body.getPosition().dst2(position) < 256f) {
                otherBody = body;
                break;
            }
        }
        return otherBody == null;
    }

    public Ship endGame() {
        if (ships.size() == 1) {
            return ships.get(0);
        }
        return null;
    }
}
