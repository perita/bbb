package com.peritasoft.bbb;

import com.badlogic.gdx.math.MathUtils;
import com.peritasoft.bbb.messages.IslandDef;
import com.peritasoft.bbb.messages.MapDef;

import java.util.Random;

public class PiecemealMapDefGenerator {
    private final double zoom;

    public PiecemealMapDefGenerator() {
        this(25.0);
    }

    public PiecemealMapDefGenerator(double zoom) {
        this.zoom = zoom;
    }

    public MapDef generate(int width, int height) {
        double targetArea = width * height * 0.5;
        double filledArea = 0.0;
        MapDef map = new MapDef(width, height);
        while (filledArea < targetArea) {
            int newArea = 0;
            for (int i = 0; i <= 25; i++) {
                IslandDef island = generateIsland();
                float x = (float) MathUtils.random(-island.getSize() / 2, width - island.getSize() / 2);
                float y = (float) MathUtils.random(-island.getSize() / 2, height - island.getSize() / 2);
                island.moveTo(x, y);
                if (map.canPlace(island)) {
                    newArea = island.getSize() * island.getSize();
                    map.place(island);
                    break;
                }
            }
            if (newArea == 0) {
                targetArea *= 0.9;
            } else {
                filledArea += newArea;
            }
        }
        return map;
    }

    private IslandDef generateIsland() {
        return new IslandDef(MathUtils.random(50, 300), new Random().nextLong(), zoom);
    }
}