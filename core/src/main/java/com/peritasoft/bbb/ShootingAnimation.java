package com.peritasoft.bbb;

import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.Array;

public class ShootingAnimation {
    private static final int TILE_SIZE = 36;

    private final Vector2[] positions;
    private final Animation<TextureRegion> animation;
    private float stateTime;
    private final float angle;

    public ShootingAnimation(final Ship ship, Texture sheet) {
        animation = new Animation<>(0.15f, split(sheet));
        stateTime = 0f;
        angle = ship.getAngle();

        final float margin = 5f;
        final float offset = 18f;
        final Vector2 direction = new Vector2(1f, 0f).rotate(angle);
        final Vector2 perpendicular = new Vector2(-direction.y, direction.x);
        positions = new Vector2[]{
                ship.getPosition().cpy().add(
                        direction.x * offset,
                        direction.y * offset
                ).add(perpendicular.x * margin, perpendicular.y * margin),
                ship.getPosition().cpy().sub(
                        direction.x * offset,
                        direction.y * offset
                ).sub(perpendicular.x * margin, perpendicular.y * margin),
        };
    }

    public boolean draw(float delta, Batch batch) {
        stateTime += delta;
        final TextureRegion frame = animation.getKeyFrame(stateTime);
        batch.draw(
                frame,
                positions[0].x, positions[0].y,
                0f, 0f,
                (float) TILE_SIZE, (float) TILE_SIZE,
                1f, 1f,
                angle + 90f
        );

        batch.draw(
                frame,
                positions[1].x, positions[1].y,
                0f, 0f,
                (float) TILE_SIZE, (float) TILE_SIZE,
                1f, 1f,
                angle - 90f
        );
        return animation.isAnimationFinished(stateTime);
    }

    private static Array<TextureRegion> split(Texture sheet) {
        final Array<TextureRegion> regions = new Array<>();
        for (int t = 0; t < sheet.getWidth(); t += TILE_SIZE) {
            regions.add(new TextureRegion(sheet, t, 0, TILE_SIZE, TILE_SIZE));
        }
        return regions;
    }
}
