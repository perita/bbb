package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Animation;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class PeritaSoftScreen extends ScreenAdapter {
    private static final float TIMER = 3f;
    private static final int TILE_SIZE = 64;
    private static final int TILE_SCALE = 3;

    private final BoatBoomBoom game;
    private final OrthographicCamera cam;
    private final Viewport viewport;
    private final Animation<TextureRegion> animatedLogo;
    private final BitmapFont font;
    private float screenTime;
    private float titleTime;
    private float stateTime;

    public PeritaSoftScreen(final BoatBoomBoom game) {
        this.game = game;
        cam = new OrthographicCamera();
        viewport = new ScreenViewport(cam);
        animatedLogo = new Animation<>(0.10f, split(new Texture("PeritaAnimated.png")));
        font = new BitmapFont(Gdx.files.internal("vinque.fnt"), Gdx.files.internal("vinque.png"), false);
        screenTime = 0f;
        titleTime = 0f;
        stateTime = 0f;
    }


    @Override
    public void render(float delta) {
        update(delta);
        draw(game.getBatcher());
    }

    private void update(float delta) {
        if (screenTime < TIMER) {
            screenTime += delta;
            titleTime += delta;
            if (titleTime / 2 >= 1) stateTime += delta;
        } else {
            game.setScreen(new StartMenuScreen(game));
        }
    }

    private void draw(Batch batch) {
        float alpha = titleTime / 2f;
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        viewport.apply();
        show();
        batch.begin();

        final TextureRegion frame = animatedLogo.getKeyFrame(stateTime);
        batch.draw(
                frame,
                (viewport.getWorldWidth() / 2f) - ((float) TILE_SIZE / 2), (viewport.getWorldHeight() / 2f) + 50f,
                (float) TILE_SIZE / 2, (float) TILE_SIZE / 2,
                (float) TILE_SIZE, TILE_SIZE,
                (float) TILE_SCALE, TILE_SCALE,
                0f
        );

        font.getData().setScale(3f);
        font.setColor(0.6f, 0.9f, 0.31f, alpha);
        font.draw(batch, "Perita Soft",
                0f, (viewport.getWorldHeight()) / 2f,
                viewport.getWorldWidth(), 1, true);

        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        viewport.update(width, height);
    }

    @Override
    public void show() {
        cam.setToOrtho(false);
        cam.update();
        game.getBatcher().setProjectionMatrix(cam.combined);
    }

    private static Array<TextureRegion> split(Texture sheet) {
        final Array<TextureRegion> regions = new Array<>();
        for (int t = 0; t < sheet.getWidth(); t += TILE_SIZE) {
            regions.add(new TextureRegion(sheet, t, 0, TILE_SIZE, TILE_SIZE));
        }
        regions.add(regions.get(0));
        return regions;
    }
}