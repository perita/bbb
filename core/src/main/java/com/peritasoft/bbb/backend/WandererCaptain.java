package com.peritasoft.bbb.backend;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;
import com.peritasoft.bbb.AbstractGamePlay;
import com.peritasoft.bbb.Ship;
import com.peritasoft.bbb.Steerable;
import com.peritasoft.bbb.ai.Box2DLocation;
import com.peritasoft.bbb.ai.Box2DRaycastCollisionDetector;
import com.peritasoft.bbb.ai.FrontRays;
import com.peritasoft.bbb.ai.LateralRays;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.TurnSide;

public class WandererCaptain implements Captain {
    private final CaptainOrders orders = new CaptainOrders();
    private final FrontRays frontRays;
    private final LateralRays lateralRays;
    private TurnSide prevTurnSide = TurnSide.NONE;
    private Steerable boat;

    public WandererCaptain(final World world) {
        orders.setAdvance(true);
        final Box2DRaycastCollisionDetector collisionDetector = new Box2DRaycastCollisionDetector(world);
        frontRays = new FrontRays(collisionDetector, 20f);
        lateralRays = new LateralRays(collisionDetector, 20f);
    }

    @Override
    public CaptainOrders nextOrders() {
        float velocityAngle = (Box2DLocation.convertVectorToAngle(boat.getLinearVelocity()) - MathUtils.PI / 2) * MathUtils.radiansToDegrees;
        frontRays.updateRays(boat.getPosition(), boat.getLinearVelocity());
        lateralRays.updateRays(boat.getPosition(), velocityAngle);
        giveOrders();
        return orders;
    }

    @Override
    public void join(BackendGamePlay game) {
        if (boat != null) {
            throw new IllegalStateException("This captain already joined a game");
        }
        final int id = game.addCaptain(this);
        boat = game.getBoat(id);
    }

    private void giveOrders() {
        final boolean portCollision = frontRays.isCollidingRight();
        final boolean starboardCollision = frontRays.isCollidingLeft();
        final boolean portLateral = lateralRays.isOnlyRightColliding();
        final boolean starboardLateral = lateralRays.isOnlyLeftColliding();
        if (portCollision) {
            if (starboardCollision) {
                if (portLateral) {
                    hardToStarboard();
                } else if (starboardLateral) {
                    hardToPort();
                } else if (prevTurnSide == TurnSide.STARBOARD) {
                    hardToStarboard();
                } else {
                    hardToPort();
                }
            } else {
                if (portLateral) {
                    hardToStarboard();
                } else {
                    hardToPort();
                }
            }
        } else {
            if (starboardCollision) {
                if (starboardLateral) {
                    hardToPort();
                } else {
                    hardToStarboard();
                }
            } else {
                steadyAsSheGoes();
            }
        }
    }

    private void steadyAsSheGoes() {
        orders.setTurnSide(TurnSide.NONE);
        frontRays.growRays(100f * AbstractGamePlay.UPDATE_STEP);
    }

    private void hardToPort() {
        orders.setTurnSide(TurnSide.PORT);
        frontRays.turnRight();
        prevTurnSide = orders.getTurnSide();
    }

    private void hardToStarboard() {
        orders.setTurnSide(TurnSide.STARBOARD);
        frontRays.turnLeft();
        prevTurnSide = orders.getTurnSide();
    }

    public void comandeer(final Ship ship) {
        this.boat = ship;
    }

    public void renderDebug(final Matrix4 projectionMatrix, final ShapeRenderer sr) {
        sr.setProjectionMatrix(projectionMatrix);
        sr.begin(ShapeRenderer.ShapeType.Line);
        frontRays.renderDebug(sr);
        lateralRays.renderDebug(sr);
        sr.end();
    }
}
