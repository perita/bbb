package com.peritasoft.bbb.backend;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.peritasoft.bbb.AbstractGamePlay;
import com.peritasoft.bbb.Boat;
import com.peritasoft.bbb.CannonBall;
import com.peritasoft.bbb.messages.CaptainOrders;
import com.peritasoft.bbb.messages.MapDef;
import com.peritasoft.bbb.messages.PlayState;

final class BackendGamePlay extends AbstractGamePlay {
    private final MapDef mapDef;
    private final PlayState state;
    private final Captain[] captains;
    private final CaptainOrders[] lastGivenOrders;
    private final Listener listener;
    private int sendUpdate = UPDATE_PERIOD;
    private static final int UPDATE_PERIOD = 2;

    BackendGamePlay(final int numPlayers, final MapDef mapDef, final Listener listener) {
        super(numPlayers, mapDef);
        this.mapDef = mapDef;
        captains = new Captain[numPlayers];
        lastGivenOrders = new CaptainOrders[numPlayers];
        this.listener = listener;
        for (Boat boat : boats) {
            boat.placeOn(map.findStartingPosition(), MathUtils.random(MathUtils.PI2));
        }
        state = new PlayState(captains.length);
    }

    int addCaptain(final Captain captain) {
        for (int i = 0; i < captains.length; i++) {
            if (captains[i] == null) {
                captains[i] = captain;
                lastGivenOrders[i] = new CaptainOrders();
                return i;
            }
        }
        throw new IllegalStateException("All captain slots are full");
    }

    PlayState snapshotState() {
        return state.snapshot(lastGivenOrders[1].getSequenceNumber(), boats, cannonBalls);
    }

    @Override
    protected void simulate(final float dt) {
        for (int i = 0; i < captains.length; i++) {
            CaptainOrders givenOrders = captains[i].nextOrders();
            boats[i].followOrders(givenOrders);
            if (givenOrders.isFire()) {
                Vector2 direction = new Vector2(1f, 0f).rotateRad(boats[i].getAngle() + MathUtils.PI / 2);
                Vector2 pos = new Vector2(boats[i].getPosition());
                freeCannonBall().reset(pos.add(direction.x * 5f, direction.y * 5f), direction, world);
                direction.scl(-1f);
                freeCannonBall().reset(pos.add(direction.x * 10f, direction.y * 10f), direction, world);
            }
            lastGivenOrders[i].set(givenOrders);
        }
        sendUpdate--;
        if (sendUpdate == 0) {
            listener.stateUpdated(snapshotState());
            sendUpdate = UPDATE_PERIOD;
        }
    }

    private CannonBall freeCannonBall() {
        for (CannonBall ball : cannonBalls) {
            if (!ball.isActive()) {
                return ball;
            }
        }
        final CannonBall ball = new CannonBall();
        cannonBalls.add(ball);
        return ball;
    }

    boolean areAllPiratesJoined() {
        for (Captain captain : captains) {
            if (captain == null) {
                return false;
            }
        }
        return true;
    }

    MapDef getMapDef() {
        return mapDef;
    }

    public interface Listener {
        void stateUpdated(PlayState state);
    }

}
