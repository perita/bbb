package com.peritasoft.bbb.backend;

import com.peritasoft.bbb.messages.CaptainOrders;

public interface Captain {
    CaptainOrders nextOrders();

    void join(final BackendGamePlay game);
}
