package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public class EndGameScreen extends ScreenAdapter {
    private final BoatBoomBoom game;
    private final GamePlayRenderer renderer;
    private final int localPlayers;
    private final OrthographicCamera cam;
    private final Viewport viewport;
    private final BitmapFont font;

    public EndGameScreen(final BoatBoomBoom game, final GamePlayRenderer renderer, int localPlayers) {
        this.game = game;
        this.renderer = renderer;
        this.localPlayers = localPlayers;
        cam = new OrthographicCamera();
        viewport = new ScreenViewport(cam);
        font = new BitmapFont(Gdx.files.internal("vinque.fnt"), Gdx.files.internal("vinque.png"), false);
    }

    @Override
    public void render(float delta) {
        update(delta);
        draw(game.getBatcher());
    }

    private void update(float delta) {
        if (Gdx.input.isKeyJustPressed(Input.Keys.ESCAPE)) {
            game.setScreen(new StartMenuScreen(game));
        } else if (Gdx.input.isKeyJustPressed(Input.Keys.ANY_KEY)) {
            game.setScreen(new GamePlayScreen(game, localPlayers));
        }
    }

    private void draw(Batch batch) {
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
        viewport.apply();
        show();
        batch.begin();
        font.getData().setScale(1.5f);
        font.setColor(1f, 0.91f, 0f, 1f);
        font.draw(batch, "Press any key for another round of Grog or ESC to end the fun",
                0f, viewport.getWorldHeight(), viewport.getWorldWidth(), 1, true);
        batch.end();
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        renderer.resize(width, height);
        viewport.update(width, height);
    }

    @Override
    public void show() {
        cam.setToOrtho(false);
        cam.update();
        game.getBatcher().setProjectionMatrix(cam.combined);
    }
}