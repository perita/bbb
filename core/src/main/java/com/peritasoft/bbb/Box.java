package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class Box {
    private final Vector2 position;
    private float lifeTime;
    private final Body body;

    public Box(final Vector2 position, final World world, float lifeTime) {
        this.position = position;
        this.lifeTime = lifeTime;
        this.body = createBody(position, new Vector2(8f, 8f), world);
    }

    public boolean update(float delta) {
        lifeTime -= delta;
        return !isAlive();
    }

    public boolean isAlive() {
        return lifeTime > 0;
    }

    public Body getBody() {
        return body;
    }

    public Vector2 getPosition() {
        return position;
    }

    public void die() {
        lifeTime = 0f;
    }

    private static Body createBody(Vector2 initialPosition, Vector2 size, World world) {
        final BodyDef def = new BodyDef();
        def.position.set(initialPosition);
        def.type = BodyDef.BodyType.StaticBody;

        final Body body = world.createBody(def);
        final PolygonShape box = new PolygonShape();
        box.setAsBox(size.x, size.y);

        final FixtureDef fixture = new FixtureDef();
        fixture.shape = box;
        fixture.isSensor = true;

        body.createFixture(fixture);
        box.dispose();
        return body;
    }
}
