package com.peritasoft.bbb.messages;

import com.badlogic.gdx.math.Vector2;
import com.peritasoft.bbb.CannonBall;

public class CannonBallState {
    private final int id;
    private final Vector2 position = new Vector2(0f, 0f);
    private final Vector2 linearVelocity = new Vector2(0f, 0f);

    @SuppressWarnings("unused")
    CannonBallState() {
        this.id = 0;
    }

    CannonBallState(final int id, final CannonBall cannonBall) {
        this.id = id;
        if (!cannonBall.isActive()) return;
        this.position.set(cannonBall.getPosition());
        this.linearVelocity.set(cannonBall.getLinearVelocity());
    }

    int getId() {
        return id;
    }

    Vector2 getPosition() {
        return position;
    }

    Vector2 getLinearVelocity() {
        return linearVelocity;
    }

}
