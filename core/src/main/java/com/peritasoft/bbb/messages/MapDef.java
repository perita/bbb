package com.peritasoft.bbb.messages;


import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;
import com.peritasoft.bbb.Map;

import java.util.ArrayList;
import java.util.List;

public class MapDef {
    private final int width;
    private final int height;
    private final List<com.peritasoft.bbb.messages.IslandDef> islands = new ArrayList<>();

    @SuppressWarnings("unused")
    MapDef() {
        width = 0;
        height = 0;
    }

    public MapDef(final int width, final int height) {
        this.width = width;
        this.height = height;
    }

    public void place(com.peritasoft.bbb.messages.IslandDef island) {
        islands.add(island);
    }

    public boolean canPlace(final com.peritasoft.bbb.messages.IslandDef island) {
        for (com.peritasoft.bbb.messages.IslandDef placed : islands) {
            if (island.distanceTo(placed) < 300f) return false;
        }
        return true;
    }

    public Map buildMap(final World world) {
        Map map = new Map(width, height);
        for (com.peritasoft.bbb.messages.IslandDef island : islands) {
            island.placeOn(map, world);
        }
        createOutsideBorders(world);
        return map;
    }

    private void createOutsideBorders(final World world) {
        BodyDef def = new BodyDef();
        def.position.set(0f, 0f);
        Body body = world.createBody(def);
        float margin = 15f;
        ChainShape fixture = new ChainShape();
        fixture.createChain(
                new float[]{
                        -margin, -margin,
                        -margin, (float) height + margin,
                        (float) width + margin, (float) height + margin,
                        (float) width + margin, -margin,
                        -margin, -margin
                }
        );
        body.createFixture(fixture, 0f);
        fixture.dispose();
    }
}
