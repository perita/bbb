package com.peritasoft.bbb.messages;

public class CaptainOrders {
    private int sequenceNumber;
    private boolean advance = false;
    private boolean fire = false;
    private TurnSide turnSide = TurnSide.NONE;

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public void setSequenceNumber(int sequenceNumber) {
        this.sequenceNumber = sequenceNumber;
    }

    public boolean isAdvance() {
        return advance;
    }

    public void setAdvance(boolean advance) {
        this.advance = advance;
    }

    public boolean isFire() {
        return fire;
    }

    public void setFire(boolean fire) {
        this.fire = fire;
    }

    public TurnSide getTurnSide() {
        if (!isAdvance()) return TurnSide.NONE;
        return turnSide;
    }

    public void setTurnSide(final TurnSide turnSide) {
        this.turnSide = turnSide;
    }

    public void set(final CaptainOrders other) {
        this.setSequenceNumber(other.getSequenceNumber());
        this.setAdvance(other.isAdvance());
        this.setFire(other.isFire());
        this.setTurnSide(other.getTurnSide());
    }
}