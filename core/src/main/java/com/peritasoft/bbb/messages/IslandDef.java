package com.peritasoft.bbb.messages;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.FloatArray;
import com.peritasoft.bbb.Map;
import com.peritasoft.bbb.OpenSimplexNoise;

public class IslandDef {
    private final int size;
    private final long seed;
    private final Vector2 position = new Vector2();
    private final Rectangle area;
    private final double zoom;

    @SuppressWarnings("unused")
    IslandDef() {
        size = 0;
        seed = 0;
        zoom = 0;
        area = Rectangle.tmp;
    }

    public IslandDef(final int size, final long seed, final double zoom) {
        this.size = size;
        this.seed = seed;
        this.zoom = zoom;
        this.area = computeArea();
    }

    private Rectangle computeArea() {
        float minX = size + 1f;
        float minY = size + 1;
        float maxX = -size - 1f;
        float maxY = -size - 1;
        final OpenSimplexNoise noise = new OpenSimplexNoise(seed);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                int height = computeHeight(noise, x, y);
                if (height > 126) {
                    minX = Math.min(minX, x);
                    minY = Math.min(minY, y);
                    maxX = Math.max(maxX, x);
                    maxY = Math.max(maxY, y);
                }
            }
        }
        return new Rectangle(minX, minY, maxX - minX, maxY - minY);
    }

    public int getSize() {
        return size;
    }

    public void moveTo(final float x, final float y) {
        position.set(x, y);
    }

    float distanceTo(final IslandDef other) {
        final float x1a = position.x + minX();
        final float x1b = position.x + maxX();
        final float y1a = position.y + minY();
        final float y1b = position.y + maxY();
        final float x2a = other.position.x + other.minX();
        final float x2b = other.position.x + other.maxX();
        final float y2a = other.position.y + other.minY();
        final float y2b = other.position.y + other.maxY();
        final boolean left = x2b < x1a;
        final boolean right = x1b < x2a;
        final boolean bottom = y2b < y1a;
        final boolean top = y1b < y2a;

        if (top && left) return Vector2.dst2(x1a, y1b, x2b, y2a);
        else if (bottom && left) return Vector2.dst2(x1a, y1a, x2b, y2b);
        else if (bottom && right) return Vector2.dst2(x1b, y1a, x2a, y2b);
        else if (top && right) return Vector2.dst2(x1b, y1b, x2a, y2a);
        else if (left) return x1a - x2b;
        else if (right) return x2a - x1b;
        else if (bottom) return y1a - y2b;
        else if (top) return y2a - y1b;
        else return 0f;
    }

    private float maxY() {
        return area.y + area.height;
    }

    private float minY() {
        return area.y;
    }

    private float maxX() {
        return area.x + area.width;
    }

    private float minX() {
        return area.x;
    }

    void placeOn(final Map map, final World world) {
        final OpenSimplexNoise noise = new OpenSimplexNoise(seed);
        for (int y = 0; y < size; y++) {
            for (int x = 0; x < size; x++) {
                int height = computeHeight(noise, x, y);
                map.set(x + (int) position.x, y + (int) position.y, height);
            }
        }

        final int partitions = 100;
        final float angle = 360f / partitions;

        final BodyDef def = new BodyDef();
        def.position.set(position.x + size / 2f, position.y + size / 2f);
        final Body body = world.createBody(def);
        final Vector2 ray = new Vector2(1f, 0f);
        final FloatArray vertices = new FloatArray(partitions * 2);
        final float halfSize = size * .5f;
        final Vector2 pos = new Vector2();
        for (int p = 0; p < partitions; p++) {
            pos.set(halfSize + ray.x * halfSize, halfSize + ray.y * halfSize);
            while (!isBorder(pos, map) && Math.abs(pos.x) < size * 2 && Math.abs(pos.y) < size * 2) {
                pos.sub(ray);
            }
            pos.sub(halfSize, halfSize);
            if (vertices.isEmpty() ||
                    pos.dst2(vertices.get(vertices.size - 2), vertices.get(vertices.size - 1)) > 0.005f * 0.005f) {
                vertices.add(pos.x);
                vertices.add(pos.y);
            }
            ray.rotate(angle).nor();
        }
        ChainShape fixture = new ChainShape();
        fixture.createChain(vertices.toArray());
        body.createFixture(fixture, 0f);
        fixture.dispose();
    }

    private boolean isBorder(final Vector2 pos, final Map map) {
        final int mapX = (int) (this.position.x + pos.x);
        final int mapY = (int) (this.position.y + pos.y);
        return pos.x >= 0 && pos.x < size &&
                pos.y >= 0 && pos.y < size &&
                mapX >= 0 && mapX < map.getWidth() &&
                mapY >= 0 && mapY < map.getHeight() &&
                map.get(mapX, mapY) > 138;
    }

    private int computeHeight(final OpenSimplexNoise noise, final int x, final int y) {
        final double halfSize = size * .5;
        final double dy = y - halfSize;
        final double dx = x - halfSize;
        final double distance = Math.sqrt(dx * dx + dy * dy);
        final double delta = distance / halfSize;
        final double gradient = delta * delta;
        final double mask = Math.max(0.0, 1.0 - gradient);
        final double value = (noise.eval(x / zoom, y / zoom, 0.0) + 1.0) / 2.0;
        return MathUtils.clamp((int) (value * mask * 255.0) + 100, 1, 254);
    }
}
