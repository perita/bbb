package com.peritasoft.bbb.messages;

import com.badlogic.gdx.math.Vector2;
import com.peritasoft.bbb.Boat;

public class PlayerState {
    private final Vector2 position = new Vector2(0f, 0f);
    private final Vector2 linearVelocity = new Vector2(0f, 0f);
    private float angle = 0f;
    private float angularVelocity = 0f;

    void snapshot(final Boat boat) {
        position.set(boat.getPosition());
        linearVelocity.set(boat.getLinearVelocity());
        angle = boat.getAngle();
        angularVelocity = boat.getAngularVelocity();
    }

    void restore(final Boat boat) {
        boat.setAngularVelocity(angularVelocity);
        boat.setLinearVelocity(linearVelocity);
        boat.placeOn(position, angle);
    }

    void interpolateBetween(final PlayerState base, final PlayerState next, final float t) {
        position.set(
                base.position.x + (next.position.x - base.position.x) * t,
                base.position.y + (next.position.y - base.position.y) * t
        );
        linearVelocity.set(
                base.linearVelocity.x + (next.linearVelocity.x - base.linearVelocity.x) * t,
                base.linearVelocity.y + (next.linearVelocity.y - base.linearVelocity.y) * t
        );
        angle = base.angle + (next.angle - base.angle) * t;
        angularVelocity = base.angularVelocity + (next.angularVelocity - base.angularVelocity) * t;
    }

    @Override
    public String toString() {
        return "BoatState{" +
                "  position=" + position +
                ", linearVelocity=" + linearVelocity +
                ", angle=" + angle +
                ", angularVelocity=" + angularVelocity +
                '}';
    }

    void target(Boat boat) {
        boat.targetTo(position, angle);
    }
}
