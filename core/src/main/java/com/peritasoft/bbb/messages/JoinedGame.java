package com.peritasoft.bbb.messages;

public class JoinedGame {
    private int playerId = -1;
    private final MapDef map;
    private final PlayState initialState;

    private static final MapDef EMPTY_MAP_DEF = new MapDef();
    private static final PlayState EMPTY_PLAYS_STATE = new PlayState();

    @SuppressWarnings("unused")
    JoinedGame() {
        map = EMPTY_MAP_DEF;
        initialState = EMPTY_PLAYS_STATE;
    }

    public JoinedGame(final MapDef map, final PlayState initialState) {
        this.map = map;
        this.initialState = initialState;
    }

    public int getPlayerId() {
        return playerId;
    }

    public void setPlayerId(int playerId) {
        this.playerId = playerId;
    }

    public MapDef getMap() {
        return map;
    }

    public PlayState getInitialState() {
        return initialState;
    }
}
