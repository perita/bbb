package com.peritasoft.bbb.messages;

import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.Array;
import com.peritasoft.bbb.Boat;
import com.peritasoft.bbb.CannonBall;

public class PlayState {
    private int sequenceNumber = 0;
    private long timestamp = System.currentTimeMillis();
    private final PlayerState[] playerStates;
    private CannonBallState[] cannonBallStates = EMPTY_CANNON_BALL_STATES;
    private static final PlayerState[] EMPTY_STATES = new PlayerState[0];
    private static final CannonBallState[] EMPTY_CANNON_BALL_STATES = new CannonBallState[0];

    @SuppressWarnings("unused")
    PlayState() {
        playerStates = EMPTY_STATES;
    }

    public PlayState(final int numBoats) {
        if (numBoats <= 0) {
            throw new IllegalArgumentException("There must be at least a player to capture its state");
        }
        playerStates = new PlayerState[numBoats];
        for (int i = 0; i < numBoats; i++) {
            playerStates[i] = new PlayerState();
        }
    }

    public static PlayState interpolateBetween(final PlayState base, final PlayState next, final float t) {
        if (base.countPlayers() != next.countPlayers()) {
            throw new IllegalArgumentException("Both status must have the same number of boats");
        }
        final PlayState interpolation = new PlayState(base.countPlayers());
        interpolation.sequenceNumber = base.sequenceNumber;
        for (int i = 0; i < interpolation.playerStates.length; i++) {
            interpolation.playerStates[i].interpolateBetween(base.playerStates[i], next.playerStates[i], t);
        }
        return interpolation;
    }

    public int getSequenceNumber() {
        return sequenceNumber;
    }

    public PlayState snapshot(final int sequenceNumber, final Boat[] boats, final Array<CannonBall> cannonBalls) {
        if (boats.length != playerStates.length) {
            throw new IllegalArgumentException("The number of boats and of players does not match");
        }
        this.timestamp = System.currentTimeMillis();
        this.sequenceNumber = sequenceNumber;
        for (int i = 0; i < playerStates.length; i++) {
            playerStates[i].snapshot(boats[i]);
        }
        final Array<CannonBallState> activeCannonBalls = new Array<>(false, cannonBalls.size);
        for (int i = 0; i < cannonBalls.size; i++) {
            CannonBall ball = cannonBalls.get(i);
            if (ball.isActive()) {
                activeCannonBalls.add(new CannonBallState(i, ball));
            }
        }
        if (activeCannonBalls.size > 0) {
            cannonBallStates = activeCannonBalls.toArray(CannonBallState.class);
        } else {
            cannonBallStates = EMPTY_CANNON_BALL_STATES;
        }
        return this;
    }

    public void restore(final Boat[] boats) {
        if (boats.length != playerStates.length) {
            throw new IllegalArgumentException("The number of boats and of states do not match");
        }
        for (int i = 0; i < boats.length; i++) {
            restoreOnly(boats, i);
        }
    }

    public void restore(final Array<CannonBall> cannonBalls, final World world) {
        for (CannonBallState state : cannonBallStates) {
            while (state.getId() >= cannonBalls.size) {
                cannonBalls.add(new CannonBall());
            }
            CannonBall ball = cannonBalls.get(state.getId());
            if (!ball.isActive()) {
                ball.reset(state.getPosition(), state.getLinearVelocity(), world);
            }
            ball.setPosition(state.getPosition());
            ball.setLinearVelocity(state.getLinearVelocity());
        }
    }

    public void restoreAllBut(final Boat[] boats, final int boatIndex) {
        if (boats.length != playerStates.length) {
            throw new IllegalArgumentException("The number of boats and of states do not match");
        }
        for (int i = 0; i < boats.length; i++) {
            if (i == boatIndex) {
                continue;
            }
            restoreOnly(boats, i);
        }
    }

    public void restoreOnly(final Boat[] boats, final int boatIndex) {
        playerStates[boatIndex].restore(boats[boatIndex]);
    }

    public void target(final Boat[] boats) {
        if (boats.length != playerStates.length) {
            throw new IllegalArgumentException("The number of boats and of states do not match");
        }
        for (int i = 0; i < boats.length; i++) {
            playerStates[i].target(boats[i]);
        }
    }

    public long getTimestamp() {
        return timestamp;
    }

    public int countPlayers() {
        return playerStates.length;
    }

    public int x() {
        return cannonBallStates.length;
    }
}
