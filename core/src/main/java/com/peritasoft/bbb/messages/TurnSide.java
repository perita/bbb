package com.peritasoft.bbb.messages;

public enum TurnSide {
    NONE,
    PORT,
    STARBOARD
}
