package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.ScreenAdapter;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.peritasoft.bbb.ai.WandererSailor;

import java.util.ArrayList;

public class StartMenuScreen extends ScreenAdapter {
    private static final float MENUOFFSET = 35f;
    private static final float ARROWYOFFSET = 15f;
    private static final float ARROWXOFFSET = 250f;
    private static final float BUTTONWIDTH = 430f;
    private static final float BUTTONHEIGHT = 65f;

    private final BoatBoomBoom game;

    private final GamePlay play;
    private final GamePlayRenderer renderer;
    private final ShapeRenderer shapeRenderer;

    private final OrthographicCamera cam;
    private final Viewport viewport;
    private final BitmapFont font;

    private final boolean[] selectedOption;

    public StartMenuScreen(final BoatBoomBoom game) {
        this.game = game;

        shapeRenderer = new ShapeRenderer();

        cam = new OrthographicCamera();
        viewport = new ScreenViewport(cam);
        font = new BitmapFont(Gdx.files.internal("vinque.fnt"), Gdx.files.internal("vinque.png"), false);

        selectedOption = new boolean[]{true, false};

        World world = new World(new Vector2(0f, 0f), true);
        PiecemealMapDefGenerator generator = new PiecemealMapDefGenerator();
        final Map map = generator.generate(800, 800).buildMap(world);
        final ArrayList<Ship> ships = new ArrayList<>();
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 0, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 1, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 2, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 0, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 1, world));
        ships.add(new Ship(map.findStartingPosition(), new WandererSailor(world), 2, world));
        play = new GamePlay(world, map, ships);
        world.setContactListener(new ContactResponser(play));
        renderer = new GamePlayRenderer(game.getBatcher(), play, 1, ships.get(0));
    }

    @Override
    public void render(float delta) {
        update(delta);
        draw(game.getBatcher());
    }

    private void update(float delta) {
        play.update(Math.min(0.25f, delta));
        if (Gdx.input.isKeyJustPressed(Input.Keys.DPAD_UP) || Gdx.input.isKeyJustPressed(Input.Keys.DPAD_DOWN)) {
            if (selectedOption[0]) {
                selectedOption[0] = false;
                selectedOption[1] = true;
            } else {
                selectedOption[0] = true;
                selectedOption[1] = false;
            }
        }
        if (Gdx.input.isKeyJustPressed(Input.Keys.ENTER) || Gdx.input.isKeyJustPressed(Input.Keys.SPACE)) {
            if (selectedOption[0]) {
                game.setScreen(new GamePlayScreen(game, 1));
            } else {
                game.setScreen(new GamePlayScreen(game, 2));
            }
        }
    }

    private void draw(Batch batch) {
        float optionOffset = 0f;
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
        renderer.render();
        viewport.apply();
        show();
        darkenScreen(viewport);
        //fletxa
        if (selectedOption[0]) optionOffset = 0f;
        if (selectedOption[1]) optionOffset = -75f;
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1f, 1f, 1f, 1f);
        shapeRenderer.triangle(
                (viewport.getWorldWidth() / 2) - ARROWXOFFSET, (viewport.getWorldHeight() / 2) - ARROWYOFFSET + optionOffset,
                (viewport.getWorldWidth() / 2) - ARROWXOFFSET, (viewport.getWorldHeight() / 2) - ARROWYOFFSET + 30f + optionOffset,
                (viewport.getWorldWidth() / 2) - ARROWXOFFSET + 30f, (viewport.getWorldHeight() / 2) - ARROWYOFFSET + 15f + optionOffset
        );

        //buttonBox
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.setColor(0.5f, 0.5f, 0.6f, 0.35f);
        shapeRenderer.rect(
                (viewport.getWorldWidth() / 2) - (BUTTONWIDTH / 2),
                (viewport.getWorldHeight() / 2) - (BUTTONHEIGHT / 2),
                BUTTONWIDTH, BUTTONHEIGHT
        );
        shapeRenderer.rect(
                (viewport.getWorldWidth() / 2) - (BUTTONWIDTH / 2),
                (viewport.getWorldHeight() / 2) - (BUTTONHEIGHT / 2) - 75f,
                BUTTONWIDTH, BUTTONHEIGHT
        );
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.begin();
        font.getData().setScale(2.75f);
        font.setColor(1f, 0.91f, 0f, 1f);
        font.draw(batch, "Boom Boom Boat",
                0f, viewport.getWorldHeight(),
                viewport.getWorldWidth(), 1, true);


        font.getData().setScale(1.5f);
        font.setColor(1f, 1f, 1f, 1f);
        font.draw(batch, "One local pirate",
                0f, (viewport.getWorldHeight() / 2) + MENUOFFSET,
                viewport.getWorldWidth(), 1, false);

        font.getData().setScale(1.5f);
        font.setColor(1f, 1f, 1f, 1f);
        font.draw(batch, "Two local pirates",
                0f, (viewport.getWorldHeight() / 2) - MENUOFFSET,
                viewport.getWorldWidth(), 1, false);
        batch.end();
    }

    private void darkenScreen(Viewport viewport) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        viewport.apply();
        final Camera viewCam = viewport.getCamera();
        shapeRenderer.setProjectionMatrix(viewCam.combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0f, 0f, 0f, 0.25f);
        shapeRenderer.rect(
                (viewCam.position.x - (viewCam.viewportWidth / 2f)),
                (viewCam.position.y - (viewCam.viewportHeight / 2f)),
                viewCam.viewportWidth, viewCam.viewportHeight
        );
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    @Override
    public void resize(int width, int height) {
        super.resize(width, height);
        renderer.resize(width, height);
        viewport.update(width, height);
    }

    @Override
    public void show() {
        cam.setToOrtho(false);
        cam.update();
        game.getBatcher().setProjectionMatrix(cam.combined);
    }
}
