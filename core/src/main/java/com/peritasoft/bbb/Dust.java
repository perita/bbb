package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;

public class Dust {
    private final Vector2 position;
    private float life;
    private float scale;
    private float tempDiv;

    public Dust(final Vector2 position) {
        this.position = position;
        life = 1f;
        scale = 1f;
        tempDiv = 4f;
    }

    public Vector2 getPosition() {
        return position;
    }

    protected void setLife(float life) {
        this.life = life;
    }

    public float getScale() {
        return scale;
    }

    protected void setScale(float scale) {
        this.scale = scale;
    }

    protected void setTempDiv(float tempDiv) {
        this.tempDiv = tempDiv;
    }

    public boolean update(float delta) {
        life -= delta;
        scale -= delta / tempDiv;
        return life <= 0f;
    }
}
