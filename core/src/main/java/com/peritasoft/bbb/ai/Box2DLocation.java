package com.peritasoft.bbb.ai;

import com.badlogic.gdx.ai.utils.Location;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Box2DLocation implements Location<Vector2> {
    private final Vector2 position;
    private float orientation;

    public Box2DLocation() {
        position = new Vector2();
        orientation = 0f;
    }

    @Override
    public Location<Vector2> newLocation() {
        return new Box2DLocation();
    }

    @Override
    public float getOrientation() {
        return orientation;
    }

    @Override
    public Vector2 getPosition() {
        return position;
    }

    @Override
    public Vector2 angleToVector(Vector2 outVector, float angle) {
        return convertAngleToVector(outVector, angle);
    }

    @Override
    public float vectorToAngle(Vector2 vector) {
        return convertVectorToAngle(vector);
    }

    @Override
    public void setOrientation(float orientation) {
        this.orientation = orientation;
    }

    public static Vector2 convertAngleToVector(Vector2 outVector, float angle) {
        if (outVector != null) {
            outVector.set(-MathUtils.sin(angle), MathUtils.cos(angle));
        }
        return outVector;
    }

    public static float convertVectorToAngle(Vector2 vector) {
        return vector == null ? 0f : MathUtils.atan2(-vector.x, vector.y);
    }
}
