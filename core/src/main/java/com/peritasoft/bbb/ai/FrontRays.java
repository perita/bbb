package com.peritasoft.bbb.ai;

import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class FrontRays {
    private static final float HALF_PI = MathUtils.PI * 0.5f;

    private static final int RIGHT_RAY = 0;
    private static final int LEFT_RAY = 1;

    private static final float LONG_RAY_LENGTH = 250f;
    private static final float SHORT_RAY_LENGTH = LONG_RAY_LENGTH / 2f;

    private final RaycastCollisionDetector<Vector2> collisionDetector;
    private final float offset;
    private final Ray[] rays;

    public FrontRays(RaycastCollisionDetector<Vector2> collisionDetector, float offset) {
        this.collisionDetector = collisionDetector;
        this.offset = offset;
        rays = new Ray[]{
                new Ray(-HALF_PI),
                new Ray(HALF_PI)
        };
    }

    public boolean isCollidingRight() {
        return getRightRay().isColliding();
    }

    public boolean isCollidingLeft() {
        return getLeftRay().isColliding();
    }

    private Ray getRightRay() {
        return rays[RIGHT_RAY];
    }

    private Ray getLeftRay() {
        return rays[LEFT_RAY];
    }

    public void updateRays(Vector2 position, Vector2 linearVelocity) {
        final float velocityAngle = Box2DLocation.convertVectorToAngle(linearVelocity);
        for (Ray ray : rays) {
            ray.updatePosition(position, linearVelocity, velocityAngle, offset);
            ray.checkCollision(position, collisionDetector);
        }
    }

    public void growRays(float delta) {
        for (Ray ray : rays) {
            ray.grow(delta);
        }
    }

    public void turnRight() {
        getRightRay().elongate();
        getLeftRay().shorten();
    }

    public void turnLeft() {
        getRightRay().shorten();
        getLeftRay().elongate();
    }

    public void renderDebug(ShapeRenderer shapeRenderer) {
        for (Ray ray : rays) {
            ray.renderDebug(shapeRenderer);
        }
    }

    private class Ray extends com.badlogic.gdx.ai.utils.Ray<Vector2> {

        private final float angleOffset;
        private boolean isColliding;
        private float distance;
        private final Collision<Vector2> collision;
        private float length;

        Ray(final float angleOffset) {
            super(new Vector2(), new Vector2());
            this.angleOffset = angleOffset;
            this.collision = new Collision<>(new Vector2(), new Vector2());
            this.isColliding = false;
            this.distance = Float.MAX_VALUE;
            this.length = LONG_RAY_LENGTH;
        }

        private boolean isColliding() {
            return isColliding;
        }

        private float getDistance() {
            return distance;
        }

        private void updatePosition(Vector2 position, Vector2 linearVelocity, float angle, float offset) {
            final Vector2 v = Box2DLocation.convertAngleToVector(start, angle + angleOffset);
            if (v != null) v.scl(offset).add(position);
            end.set(linearVelocity).nor().scl(length).add(start);
        }

        private void checkCollision(Vector2 position, RaycastCollisionDetector<Vector2> collisionDetector) {
            isColliding = collisionDetector.findCollision(collision, this);
            distance = isColliding ? position.dst(collision.point) : Float.MAX_VALUE;
        }

        private void grow(float delta) {
            length = Math.min(length + delta, LONG_RAY_LENGTH);
        }

        private void elongate() {
            length = LONG_RAY_LENGTH;
        }

        private void shorten() {
            length = SHORT_RAY_LENGTH;
        }

        private void renderDebug(ShapeRenderer shapeRenderer) {
            if (isColliding) {
                shapeRenderer.setColor(1f, 0f, 0f, 1f);
                float dst = distance / length;
                shapeRenderer.setColor(1f - dst, dst, 0f, 1f);
                shapeRenderer.circle(collision.point.x, collision.point.y, 10f);
            } else {
                shapeRenderer.setColor(0f, 1f, 0f, 1f);
            }
            shapeRenderer.line(start, end);
        }
    }
}
