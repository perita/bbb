package com.peritasoft.bbb.ai;

import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.Ray;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.RayCastCallback;
import com.badlogic.gdx.physics.box2d.World;

public class Box2DRaycastCollisionDetector implements RaycastCollisionDetector<Vector2> {

    private final World world;
    private final Box2DRayCastCallback callback;

    public Box2DRaycastCollisionDetector(World world) {
        this.world = world;
        callback = new Box2DRayCastCallback();
    }

    @Override
    public boolean collides(Ray<Vector2> ray) {
        return findCollision(null, ray);
    }

    @Override
    public boolean findCollision(Collision<Vector2> outputCollision, Ray<Vector2> inputRay) {
        if (inputRay == null) return false;
        if (inputRay.start.epsilonEquals(inputRay.end, MathUtils.FLOAT_ROUNDING_ERROR)) return false;
        callback.outputCollision = outputCollision;
        callback.collided = false;
        world.rayCast(callback, inputRay.start, inputRay.end);
        return callback.collided;
    }

    private static class Box2DRayCastCallback implements RayCastCallback {
        Collision<Vector2> outputCollision;
        boolean collided;

        @Override
        public float reportRayFixture(Fixture fixture, Vector2 point, Vector2 normal, float fraction) {
            if (outputCollision != null) {
                outputCollision.set(point, normal);
            }
            collided = collided ||
                    fixture == null ||
                    fixture.getBody() == null ||
                    fixture.getBody().getUserData() == null;
            return fraction;
        }

    }
}
