package com.peritasoft.bbb.ai;

import com.badlogic.gdx.ai.utils.Collision;
import com.badlogic.gdx.ai.utils.RaycastCollisionDetector;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class LateralRays {
    private static final float RAY_LENGTH = 150f;
    private static final int LEFT_RAY = 0;
    private static final int RIGHT_RAY = 3;

    private final RaycastCollisionDetector<Vector2> collisionDetector;
    private final float offset;
    private final Ray[] rays;

    public LateralRays(RaycastCollisionDetector<Vector2> collisionDetector, float offset) {
        this.collisionDetector = collisionDetector;
        this.offset = offset;
        rays = new Ray[]{
                new Ray(0f),
                new Ray(0f + MathUtils.PI / 6),
                new Ray(0f + MathUtils.PI / 3),
                new Ray(MathUtils.PI),
                new Ray(MathUtils.PI - MathUtils.PI / 6),
                new Ray(MathUtils.PI - MathUtils.PI / 3)
        };
    }

    public float getOffset() {
        return offset;
    }

    private Ray getRightRay() {
        return rays[RIGHT_RAY];
    }

    private Ray getLeftRay() {
        return rays[LEFT_RAY];
    }

    public void renderDebug(ShapeRenderer shapeRenderer) {
        for (Ray ray : rays) {
            ray.renderDebug(shapeRenderer);
        }
    }

    public void updateRays(Vector2 position, float angle) {
        float radAngle = MathUtils.degreesToRadians * angle;
        for (Ray ray : rays) {
            ray.updatePosition(position, radAngle);
            ray.checkCollision(position, collisionDetector);
        }
    }

    private boolean sideCollision(int side) {
        for (int i = 0; i <= 2; i++) {
            if (rays[side + i].isColliding) return true;
        }
        return false;
    }

    private boolean rightSideCollision() {
        return sideCollision(RIGHT_RAY);
    }

    private boolean leftSideCollision() {
        return sideCollision(LEFT_RAY);
    }

    public boolean isOnlyRightColliding() {
        return rightSideCollision() && !leftSideCollision();
    }

    public boolean isOnlyLeftColliding() {
        return leftSideCollision() && !rightSideCollision();
    }

    private class Ray extends com.badlogic.gdx.ai.utils.Ray<Vector2> {
        private final float offset;
        private boolean isColliding;
        private float distance;
        private final Collision<Vector2> collision;

        private Ray(float offset) {
            super(new Vector2(), new Vector2());
            this.offset = offset;
            this.isColliding = false;
            this.distance = Float.MAX_VALUE;
            collision = new Collision<>(new Vector2(), new Vector2());
        }

        private void updatePosition(Vector2 position, float angle) {
            final Vector2 v1 = Box2DLocation.convertAngleToVector(start, angle + this.offset);
            if (v1 != null) v1.scl(20f).add(position);
            final Vector2 v2 = Box2DLocation.convertAngleToVector(end, angle + this.offset);
            if (v2 != null) v2.scl(20f + RAY_LENGTH).add(position);
        }

        private void checkCollision(Vector2 position, RaycastCollisionDetector<Vector2> collisionDetector) {
            isColliding = collisionDetector.findCollision(collision, this);
            distance = isColliding ? position.dst(collision.point) : Float.MAX_VALUE;
        }

        private void renderDebug(ShapeRenderer shapeRenderer) {
            if (isColliding) {
                float dst = distance / (RAY_LENGTH + 20f);
                shapeRenderer.setColor(1f - dst, dst, 0f, 1f);
                shapeRenderer.circle(collision.point.x, collision.point.y, 10f);
            } else {
                shapeRenderer.setColor(0f, 1f, 0f, 1f);
            }
            shapeRenderer.line(start, end);
        }
    }
}
