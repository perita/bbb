package com.peritasoft.bbb.ai;

import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.physics.box2d.World;
import com.peritasoft.bbb.Sailor;
import com.peritasoft.bbb.Ship;
import com.peritasoft.bbb.backend.WandererCaptain;
import com.peritasoft.bbb.messages.CaptainOrders;

public class WandererSailor implements Sailor {

    private final float RIGHT_ROTATION = 1f;
    private final float LEFT_ROTATION = -RIGHT_ROTATION;

    private Ship ship;
    private ShapeRenderer shapeRenderer;
    private final WandererCaptain captain;
    private CaptainOrders orders;

    public WandererSailor(final World world) {
        shapeRenderer = null;
        captain = new WandererCaptain(world);
        orders = new CaptainOrders();
    }

    @Override
    public Ship getShip() {
        return ship;
    }

    @Override
    public void setShip(Ship ship) {
        this.ship = ship;
    }

    @Override
    public void update(float delta) {
        captain.comandeer(ship);
        orders = captain.nextOrders();
    }

    @Override
    public float thrust() {
        return orders.isAdvance() ? 1f : 0f;
    }

    @Override
    public float rotation() {
        switch (orders.getTurnSide()) {
            case STARBOARD:
                return LEFT_ROTATION;
            case PORT:
                return RIGHT_ROTATION;
            default:
                return 0f;
        }
    }

    @Override
    public void renderDebug(Matrix4 projectionMatrix) {
        if (shapeRenderer == null) {
            shapeRenderer = new ShapeRenderer();
        }
        captain.renderDebug(projectionMatrix, shapeRenderer);
    }

    @Override
    public boolean shootCanon() {
        return false;
    }
}