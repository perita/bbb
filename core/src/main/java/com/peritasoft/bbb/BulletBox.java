package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.World;

public class BulletBox extends Box {
    public BulletBox(Vector2 position, World world, float lifeTime) {
        super(position, world, lifeTime);
        getBody().setUserData(this);
    }
}
