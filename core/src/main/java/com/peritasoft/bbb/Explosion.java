package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;

public class Explosion extends Dust {

    public Explosion(Vector2 position) {
        super(position);
        setLife(0.2f);
        setScale(1f);
        setTempDiv(1f);
    }
}
