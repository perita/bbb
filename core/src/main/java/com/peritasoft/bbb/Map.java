package com.peritasoft.bbb;

import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;

public class Map {
    private static final int OPEN_WATER_AREA = 64;
    private static final int OPEN_WATER_LEVEL = 116;

    private final int width;
    private final int height;
    private final int[] values;

    public Map(int width, int height) {
        this.width = width;
        this.height = height;
        values = new int[width * height];
    }

    public int getWidth() {
        return width;
    }

    public int getHeight() {
        return height;
    }


    public void set(int x, int y, int value) {
        if (x >= 0 && x < width && y >= 0 && y < height) {
            values[(height - y - 1) * width + x] = value;
        }
    }

    public int get(int x, int y) {
        return values[(height - y - 1) * width + x];
    }

    public Vector2 findStartingPosition() {
        while (true) {
            int x = MathUtils.random(OPEN_WATER_AREA, width - OPEN_WATER_AREA);
            int y = MathUtils.random(OPEN_WATER_AREA, height - OPEN_WATER_AREA);
            if (isOpenWater(x, y)) {
                return new Vector2((float) x, (float) y);
            }
        }
    }

    private boolean isOpenWater(int posX, int posY) {
        for (int y = (posY + OPEN_WATER_AREA / 2); y >= (posY - OPEN_WATER_AREA / 2); y--) {
            for (int x = (posX - OPEN_WATER_AREA / 2); x < (posX + OPEN_WATER_AREA / 2); x++) {
                if (get(x, y) > OPEN_WATER_LEVEL) return false;
            }
        }
        return true;
    }
}