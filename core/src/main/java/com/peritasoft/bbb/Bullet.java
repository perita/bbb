package com.peritasoft.bbb;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.*;

public class Bullet {
    private static final float SPEED = 200f;
    private static final float MAXLIFETIME = 1f;

    private final Vector2 initialPosition;
    private final Body body;
    private float lifeTime;
    private boolean killed;

    public Bullet(final Vector2 initialPosition, final Vector2 direction, final World world) {
        this.initialPosition = initialPosition;
        body = createBody(this, initialPosition, direction, world);
        lifeTime = 0f;
        killed = false;
    }

    public Body getBody() {
        return body;
    }

    public boolean getKilled() {
        return killed;
    }

    public Vector2 getInitialPosition() {
        return initialPosition;
    }

    public Vector2 getPosition() {
        return body.getPosition();
    }

    public boolean update(float delta) {
        lifeTime += delta;
        return !isAlive();
    }

    public boolean isAlive() {
        return lifeTime < MAXLIFETIME;
    }

    public void die() {
        lifeTime = MAXLIFETIME;
        killed = true;
    }

    private static Body createBody(final Bullet bullet, final Vector2 initialPosition, final Vector2 direction, final World world) {
        final BodyDef def = new BodyDef();
        def.position.set(initialPosition);
        def.type = BodyDef.BodyType.DynamicBody;
        def.linearVelocity.set(direction.x * SPEED, direction.y * SPEED);

        final Body body = world.createBody(def);
        body.setUserData(bullet);

        CircleShape circle = new CircleShape();
        circle.setRadius(3f);

        FixtureDef fixture = new FixtureDef();
        fixture.shape = circle;
        fixture.density = 0.4f;
        fixture.isSensor = false;
        fixture.restitution = 0.2f;

        body.createFixture(fixture);
        circle.dispose();
        return body;
    }
}
