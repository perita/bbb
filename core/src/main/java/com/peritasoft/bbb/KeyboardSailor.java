package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.math.Matrix4;

public class KeyboardSailor implements Sailor {
    private final Keys keys;
    private Ship ship;

    public KeyboardSailor(Keys keys) {
        this.keys = keys;
    }

    @Override
    public Ship getShip() {
        return ship;
    }

    @Override
    public void setShip(Ship ship) {
        this.ship = ship;
    }

    @Override
    public void update(float delta) {
        // Nothing to do
    }

    @Override
    public float thrust() {
        return Gdx.input.isKeyPressed(keys.thrust) ? 1.0f : 0.0f;
    }

    @Override
    public float rotation() {
        if (!Gdx.input.isKeyPressed(keys.thrust)) return 0f;
        if (Gdx.input.isKeyPressed(keys.rotateLeft)) return 1.0f;
        if (Gdx.input.isKeyPressed(keys.rotateRight)) return -1.0f;
        return 0.0f;
    }

    @Override
    public boolean shootCanon() {
        return Gdx.input.isKeyJustPressed(keys.shoot);
    }

    @Override
    public void renderDebug(Matrix4 projectionMatrix) {
        // Nothing to do
    }

    public static class Keys {
        public final int thrust;
        public final int rotateRight;
        public final int rotateLeft;
        public final int shoot;

        public Keys(int thrust, int rotateRight, int rotateLeft, int shoot) {
            this.thrust = thrust;
            this.rotateRight = rotateRight;
            this.rotateLeft = rotateLeft;
            this.shoot = shoot;
        }
    }
}
