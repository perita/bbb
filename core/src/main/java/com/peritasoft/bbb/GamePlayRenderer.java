package com.peritasoft.bbb;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Batch;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.FloatArray;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import java.util.ArrayList;
import java.util.Iterator;

public class GamePlayRenderer implements Disposable {

    private static final Array<Body> bodies = new Array<>();

    private final Batch batch;
    private final GamePlay play;
    private final int localPlayers;
    private final Ship ego;

    private final Sprite[] shipSprites;
    private final Sprite ripple;
    private final Sprite box;
    private final Sprite medibox;
    private final Sprite bullet;
    private final Sprite boom;
    private final Sprite dust;
    private final Sprite explosion;
    private final Sprite cannonUI;
    private final Sprite shieldUI;
    private final ArrayList<ShootingAnimation> shootings;
    private final Texture shootSheet;
    private final Texture background;
    private boolean debug;
    private final Box2DDebugRenderer debugRenderer;
    private final OrthographicCamera cam1;
    private final Viewport viewportL;
    private final Viewport viewportR;
    private final Viewport viewportFull;
    private final Ship player2Ship;
    private final ShapeRenderer shapeRenderer;
    private final BitmapFont font;

    public GamePlayRenderer(final Batch batch, final GamePlay play, final int localPlayers, final Ship ego) {
        this.batch = batch;
        this.play = play;
        this.localPlayers = localPlayers;
        this.ego = ego;

        shipSprites = new Sprite[]{
                new Sprite(linear(new Texture("pirateShip.png"))),
                new Sprite(linear(new Texture("englandShip.png"))),
                new Sprite(linear(new Texture("ship2sails.png")))
        };
        ripple = new Sprite(linear(new Texture("ripple.png")));
        box = new Sprite(linear(new Texture("box.png")));
        medibox = new Sprite(linear(new Texture("medibox.png")));
        bullet = new Sprite(linear(new Texture("cannonball.png")));
        boom = new Sprite(linear(new Texture("boom.png")));
        dust = new Sprite(linear(new Texture("dust.png")));
        explosion = new Sprite(linear(new Texture("explosion.png")));
        cannonUI = new Sprite(linear(new Texture("cannonballUI.png")));
        shieldUI = new Sprite(linear(new Texture("shield.png")));
        shootings = new ArrayList<>();
        shootSheet = linear(new Texture("shoot.png"));
        background = linear(new Texture(new FlatMapRenderer().renderToPixmap(play.getMap())));
        debug = false;
        debugRenderer = new Box2DDebugRenderer();
        cam1 = new OrthographicCamera();
        OrthographicCamera cam2 = new OrthographicCamera();
        viewportL = new FitViewport(800f / 2, 480f, cam1);
        viewportR = new FitViewport(800f / 2, 480f, cam2);
        viewportFull = new FitViewport(800f, 480f, cam1);
        player2Ship = play.getShips().get(1);
        shapeRenderer = new ShapeRenderer();
        font = new BitmapFont(
                Gdx.files.internal("vinque.fnt"),
                Gdx.files.internal("vinque.png"), false
        );

        cam1.setToOrtho(false);
        cam2.setToOrtho(false);
        for (Sprite sprite : shipSprites) {
            sprite.setOriginCenter();
        }
        boom.setOriginCenter();
        ripple.setOriginCenter();
        font.setUseIntegerPositions(false);
    }

    public void render() {
        if (localPlayers == 2) {
            if (player2Ship.isAlive()) {
                drawPlayerScreen(player2Ship, viewportL);
                if (play.endGame() == player2Ship) {
                    darkenPlayerScreen(viewportL, "YOU WIN");
                }
                if (player2Ship.isImmortal()) {
                    redPlayerScreen(viewportL);
                }
            } else {

                final Ship shipFollowed = player2Ship.getShipFollowed();
                drawPlayerScreen(shipFollowed == null ? player2Ship : shipFollowed, viewportL);
                darkenPlayerScreen(viewportL, "YOU LOSE");
            }
            if (ego.isAlive()) {
                drawPlayerScreen(ego, viewportR);
                if (play.endGame() == ego) {
                    darkenPlayerScreen(viewportR, "YOU WIN");
                }
                if (ego.isImmortal()) {
                    redPlayerScreen(viewportR);
                }
            } else {
                final Ship shipFollowed = ego.getShipFollowed();
                drawPlayerScreen(shipFollowed == null ? ego : shipFollowed, viewportR);
                darkenPlayerScreen(viewportR, "YOU LOSE");
            }
        } else {
            if (ego.isAlive()) {
                drawPlayerScreen(ego, viewportFull);
                if (play.endGame() == ego) {
                    darkenPlayerScreen(viewportFull, "YOU WIN");
                }
                if (ego.isImmortal()) {
                    redPlayerScreen(viewportFull);
                }
            } else {
                final Ship shipFollowed = ego.getShipFollowed();
                drawPlayerScreen(shipFollowed == null ? ego : shipFollowed, viewportFull);
                darkenPlayerScreen(viewportFull, "YOU LOSE");
            }
        }
    }

    private void drawPlayerScreen(final Ship playerShip, final Viewport viewport) {
        viewport.apply();
        Camera cam = viewport.getCamera();
        cam.position.set(
                MathUtils.clamp(playerShip.getPosition().x, cam.viewportWidth / 2.0f, play.getWidth() - cam.viewportWidth / 2.0f),
                MathUtils.clamp(
                        playerShip.getPosition().y,
                        cam.viewportHeight / 2.0f,
                        play.getHeight() - cam.viewportHeight / 2.0f
                ),
                0.0f
        );
        cam.update();
        batch.setProjectionMatrix(cam.combined);

        batch.disableBlending();
        batch.begin();
        batch.draw(background, 0.0f, 0.0f);
        batch.end();
        batch.enableBlending();

        // It uses ShapeRenderer, so it can’t be intertwined with
        // SpriteBatch, it i do not want them to be on top of
        // everything else; only the map.
        drawBulletTrails(cam);

        batch.begin();

        for (Ripple it : play.getRipples()) {
            ripple.setColor(1.0f, 1.0f, 1.0f, (it.getLife() / 15.0f));
            ripple.setScale(it.getScale());
            ripple.setOriginBasedPosition(it.getPosition().x, it.getPosition().y);
            ripple.draw(batch);
        }

        for (WaterSplash it : play.getWaterSplashes()) {
            ripple.setColor(1f, 1f, 1f, (it.life / 15));
            final FloatArray scales = it.getScales();
            for (int i = 0; i < scales.size; i++) {
                float scale = scales.get(i);
                ripple.setScale(scale);
                ripple.setOriginBasedPosition(it.getPosition().x, it.getPosition().y);
                ripple.draw(batch);
            }
        }

        final Iterator<ShootingAnimation> shootingIter = shootings.iterator();
        while (shootingIter.hasNext()) {
            final ShootingAnimation shooting = shootingIter.next();
            if (shooting.draw(Gdx.graphics.getDeltaTime(), batch)) {
                shootingIter.remove();
            }
        }

        play.getWorld().getBodies(bodies);
        for (Body body : bodies) {
            final Object obj = body.getUserData();
            if (obj instanceof Ship) draw((Ship) obj);
            else if (obj instanceof Bullet) draw((Bullet) obj);
            else if (obj instanceof MediBox) draw((MediBox) obj);
            else if (obj instanceof BulletBox) draw((BulletBox) obj);
        }
        for (Boom it : play.getBooms()) {
            boom.setRotation(it.getRotation());
            boom.setOriginBasedPosition(it.getPosition().x, it.getPosition().y);
            boom.draw(batch);
        }

        for (Dust it : play.getDusts()) {
            dust.setScale(it.getScale());
            dust.setOriginBasedPosition(it.getPosition().x, it.getPosition().y);
            dust.draw(batch);
        }

        for (Explosion it : play.getExplosions()) {
            explosion.setScale(it.getScale());
            explosion.setOriginBasedPosition(it.getPosition().x, it.getPosition().y);
            explosion.draw(batch);
        }

        batch.end();

        printPlayerUI(playerShip, viewport);

        if (debug) {
            debugRenderer.render(play.getWorld(), cam1.combined);
            playerShip.renderDebug(cam.combined);
        }
    }

    private void printPlayerUI(final Ship playerShip, final Viewport viewport) {
        batch.enableBlending();
        Gdx.gl.glEnable(GL20.GL_BLEND);
        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0f, 0f, 0f, 0.3f);
        shapeRenderer.rect(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 4f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 4f,
                94f, 24f
        );
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.begin();

        cannonUI.setOriginBasedPosition(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 27f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 16f
        );
        cannonUI.draw(batch);
        font.getData().setScale(0.4f);
        //font.setUseIntegerPositions(false)
        font.draw(
                batch,
                "x" + playerShip.getAmmo(),
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 37f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 24f
        );

        shieldUI.setOriginBasedPosition(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 68f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 16f
        );
        shieldUI.draw(batch);
        font.draw(
                batch,
                "x" + playerShip.getLife(),
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 78f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 24f
        );

        batch.end();

        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Line);
        shapeRenderer.setColor(1f, 1f, 1f, 1f);
        shapeRenderer.rect(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 8f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 7f,
                8f, 18f
        );
        shapeRenderer.end();

        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1f, 1f, 1f, 1f);
        shapeRenderer.rect(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)) + 8f,
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)) + 7f,
                8f, (playerShip.getReload() / Ship.RELOAD_TIME) * 18f
        );
        shapeRenderer.end();
    }

    private void darkenPlayerScreen(final Viewport viewport, final String textToPrint) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(0f, 0f, 0f, 0.4f);
        shapeRenderer.rect(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)),
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)),
                viewport.getCamera().viewportWidth, viewport.getCamera().viewportHeight
        );
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);

        batch.begin();
        font.getData().setScale(1f);
        font.draw(
                batch, textToPrint,
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)),
                viewport.getCamera().position.y - 15f,
                viewport.getCamera().viewportWidth, 1, false
        );
        batch.end();
    }

    private void redPlayerScreen(final Viewport viewport) {
        Gdx.gl.glEnable(GL20.GL_BLEND);
        viewport.apply();
        shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1f, 0f, 0f, 0.25f);
        shapeRenderer.rect(
                (viewport.getCamera().position.x - (viewport.getCamera().viewportWidth / 2f)),
                (viewport.getCamera().position.y - (viewport.getCamera().viewportHeight / 2f)),
                viewport.getCamera().viewportWidth, viewport.getCamera().viewportHeight
        );
        shapeRenderer.end();
        Gdx.gl.glDisable(GL20.GL_BLEND);
    }

    private void draw(final Ship ship) {
        if (ship.isAlive()) {
            shipSprites[ship.getFaction()].setRotation(ship.getAngle());
            shipSprites[ship.getFaction()].setOriginBasedPosition(ship.getPosition().x, ship.getPosition().y);
            shipSprites[ship.getFaction()].draw(batch, (1 - ship.getImmortalTime()));

            if (ship.isShooting()) {
                shootings.add(new ShootingAnimation(ship, shootSheet));
            }
        }
    }

    private void draw(final Bullet bullet) {
        this.bullet.setOriginBasedPosition(bullet.getPosition().x, bullet.getPosition().y);
        this.bullet.draw(batch);
    }

    private void draw(final BulletBox bulletBox) {
        if (bulletBox.isAlive()) {
            box.setOriginBasedPosition(bulletBox.getPosition().x, bulletBox.getPosition().y);
            box.draw(batch);
        }
    }

    private void drawBulletTrails(final Camera cam) {
        if (play.getBullets().isEmpty()) return;
        shapeRenderer.setProjectionMatrix(cam.combined);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        shapeRenderer.begin(ShapeRenderer.ShapeType.Filled);
        shapeRenderer.setColor(1.0f, 1.0f, 1.0f, 0.5f);
        for (Bullet bullet : play.getBullets()) {
            drawTrail(bullet);
        }
        shapeRenderer.end();
    }

    private void drawTrail(final Bullet bullet) {
        final Vector2 source = bullet.getInitialPosition().cpy();
        final Vector2 target = bullet.getPosition();
        final Vector2 dir = new Vector2(target).sub(source).nor();
        float distance = source.dst(target);
        final float max = 50f;
        if (distance > max) {
            source.add(dir.x * (distance - max), dir.y * (distance - max));
            distance = max;
        }
        final Vector2 mid = target.cpy().sub(dir.x * distance * 0.2f, dir.y * distance * 0.2f);
        shapeRenderer.triangle(source.x, source.y, mid.x - dir.y * 5f, mid.y + dir.x * 5f, target.x, target.y);
        shapeRenderer.triangle(source.x, source.y, mid.x + dir.y * 5f, mid.y - dir.x * 5f, target.x, target.y);
    }

    private void draw(final MediBox mediBox) {
        if (mediBox.isAlive()) {
            medibox.setOriginBasedPosition(mediBox.getPosition().x, mediBox.getPosition().y);
            medibox.draw(batch);
        }
    }

    @Override
    public void dispose() {
        background.dispose();
        ripple.getTexture().dispose();
        for (Sprite it : shipSprites) {
            it.getTexture().dispose();
        }
        box.getTexture().dispose();
        medibox.getTexture().dispose();
        bullet.getTexture().dispose();
        boom.getTexture().dispose();
        dust.getTexture().dispose();
        explosion.getTexture().dispose();
        debugRenderer.dispose();
        shootSheet.dispose();
    }

    public void followNextBoatP1() {
        int i = play.getShips().indexOf(ego.getShipFollowed());
        if (!ego.isAlive()) {
            if (i == play.getShips().size() - 1) {
                ego.setShipFollowed(play.getShips().get(0));
            } else {
                ego.setShipFollowed(play.getShips().get(i + 1));
            }
        }
    }

    public void followPrevBoatP1() {
        int i = play.getShips().indexOf(ego.getShipFollowed());
        if (!ego.isAlive()) {
            if (i == 0) {
                ego.setShipFollowed(play.getShips().get(play.getShips().size() - 1));
            } else {
                ego.setShipFollowed(play.getShips().get(i - 1));
            }
        }
    }

    public void followNextBoatP2() {
        int i = play.getShips().indexOf(player2Ship.getShipFollowed());
        if (!player2Ship.isAlive()) {
            if (i == play.getShips().size() - 1) {
                player2Ship.setShipFollowed(play.getShips().get(0));
            } else {
                player2Ship.setShipFollowed(play.getShips().get(i + 1));
            }
        }
    }

    void followPrevBoatP2() {
        int i = play.getShips().indexOf(player2Ship.getShipFollowed());
        if (!player2Ship.isAlive()) {
            if (i == 0) {
                player2Ship.setShipFollowed(play.getShips().get(play.getShips().size() - 1));
            } else {
                player2Ship.setShipFollowed(play.getShips().get(i - 1));
            }
        }
    }

    void toggleDebug() {
        debug = !debug;
    }

    public void resize(int width, int height) {
        if (localPlayers == 2) {
            viewportL.update((width / 2) - 1, height);
            viewportR.update((width / 2) - 1, height);
            viewportR.setScreenX(viewportR.getScreenX() + (width / 2) + 2);
        } else {
            viewportFull.update(width, height);
        }
    }

    private static Texture linear(Texture texture) {
        texture.setFilter(Texture.TextureFilter.Linear, Texture.TextureFilter.Linear);
        return texture;
    }
}