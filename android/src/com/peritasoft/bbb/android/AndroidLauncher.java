package com.peritasoft.bbb.android;

import android.os.Bundle;
import com.badlogic.gdx.backends.android.AndroidApplication;
import com.badlogic.gdx.backends.android.AndroidApplicationConfiguration;
import com.peritasoft.bbb.BoatBoomBoom;

public class AndroidLauncher extends AndroidApplication {

    @Override
    public void onCreate(Bundle savedInstance) {
        super.onCreate(savedInstance);
        AndroidApplicationConfiguration config = new AndroidApplicationConfiguration();
        initialize(new BoatBoomBoom(), config);
    }
}
